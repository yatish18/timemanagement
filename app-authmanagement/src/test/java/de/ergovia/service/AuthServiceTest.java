package de.ergovia.service;

import de.ergovia.controller.OutGoingAPI;
import de.ergovia.entity.EmployeeCredentialsEntity;
import de.ergovia.entity.UserRole;
import de.ergovia.repo.EmployeeCredentialsDao;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class AuthServiceTest {

    @Mock
    private EmployeeCredentialsDao dao;

    @Mock
    private OutGoingAPI outGoingAPI;

    @InjectMocks
    private AuthService service;

    @Test
    public void newEmployeePushToDataBaseIfAlreadyExists() {

        Mockito.when(dao.countAllByUsername(anyString())).thenReturn(1);
        ResponseEntity status=service.newEmployeePushToDataBase("test","test","test", UserRole.ROLE_ADMIN);
        Assert.assertSame(HttpStatus.CONFLICT,status.getStatusCode());
    }
    @Test
    public void newEmployeePushToDataBase() {

        Mockito.when(dao.countAllByUsername(anyString())).thenReturn(0);
        Mockito.when(outGoingAPI.createUserInTimeManagement(any())).thenReturn(new ResponseEntity<HttpStatus>(HttpStatus.OK));
        ResponseEntity status=service.newEmployeePushToDataBase("test","test","test", UserRole.ROLE_ADMIN);
        Assert.assertSame(HttpStatus.OK,status.getStatusCode());
    }

    @Test
    public void employeeRemoveFromDataBaseIfUserExists() {
        Mockito.when(dao.countAllByUsername(anyString())).thenReturn(1);
        Mockito.when(dao.findAllByUsername(anyString())).thenReturn(new EmployeeCredentialsEntity("testID","testName","testUsername","TestPassword",UserRole.ROLE_ADMIN));
        ResponseEntity status=service.employeeRemoveFromDataBase("testUsername");
        Assert.assertSame(HttpStatus.OK,status.getStatusCode());
    }
    @Test
    public void employeeRemoveFromDataBaseIfUserNotExists() {
        Mockito.when(dao.countAllByUsername(anyString())).thenReturn(0);
        ResponseEntity status=service.employeeRemoveFromDataBase("testUsername");
        Assert.assertSame(HttpStatus.NOT_FOUND,status.getStatusCode());
    }

}