package de.ergovia.security.jwtAuth;

import de.ergovia.AuthManagementMain;
import de.ergovia.entity.UserRole;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.http.HttpServletRequest;



@RunWith(MockitoJUnitRunner.class)
public class TokenAuthenticationServiceTest {

    private String token="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJyb2xlIjoiUk9MRV9BRE1JTiIsImlzcyI6InN0ZXBub3ZhIiwibmFtZSI6Ikt1bWFyIiwiZXhwIjoyMjQwNTIxMjAwLCJ1c2VybmFtZSI6InlhdGlzaDE4In0.IBFV_vtQ1GQseb28IJfqzP2TOye93dYN6xo9aY1Yf0sWi15_NeVmpu_j92DUa_PU1JeBpQcSgGaPpnZxuAt5XLI7bMLDrsMKa3xy_ZHPA1aCfhaCGO-0nTkU2dgwb-oGMHZ-PZboLVGvX_7ImO_G36PNLmmSIVNmkZZ4VEw3GvxRWcgY_T6lECmOGp_KkjzQKbFnaSph44ZvK_mVoCNMmDHUbU5VTJfx6hfotekw5htSjBh7CbNOwozsZl-4JFgKVzdjo1QCgFL0kfV_1YhUq1WEw6M9oEuo6LALqL2VQDVC96t0FV6q5cu87rId4afMgCQ_5RAEOKS9CMZMZkvCNA";


    private TokenAuthenticationService tokenService=new TokenAuthenticationService();

    @Test
    public void addAuthentication() {
        tokenService.initCertificate();
        JWTUser user=new JWTUser("testName","testUsername", UserRole.ROLE_ADMIN);
        MockHttpServletResponse response = new MockHttpServletResponse();
        tokenService.addAuthentication(response,user);
        Assert.assertSame(true,response.containsHeader("Authorization"));
    }

    @Test
    public void getAuthentication() {
        tokenService.initCertificate();
        MockHttpServletRequest myheader = new MockHttpServletRequest();
        myheader.addHeader("Authorization",token);
        Assert.assertSame(true,tokenService.getAuthentication(myheader).isAuthenticated());
    }
}