package de.ergovia.controller;


import de.ergovia.AuthManagementMain;
import de.ergovia.controller.receive.CreateNewEmployee;
import de.ergovia.entity.EmployeeCredentialsEntity;
import de.ergovia.entity.UserRole;

import de.ergovia.service.AuthService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.test.web.servlet.MockMvc;

import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.RequestPostProcessor;


import static org.mockito.ArgumentMatchers.anyString;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(classes=AuthManagementMain.class)
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class ControllerForOutsideTest {

    @Autowired
    private MockMvc mvc;

    private String token="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJyb2xlIjoiUk9MRV9BRE1JTiIsImlzcyI6InN0ZXBub3ZhIiwibmFtZSI6Ikt1bWFyIiwiZXhwIjoyMjQwNTIxMjAwLCJ1c2VybmFtZSI6InlhdGlzaDE4In0.IBFV_vtQ1GQseb28IJfqzP2TOye93dYN6xo9aY1Yf0sWi15_NeVmpu_j92DUa_PU1JeBpQcSgGaPpnZxuAt5XLI7bMLDrsMKa3xy_ZHPA1aCfhaCGO-0nTkU2dgwb-oGMHZ-PZboLVGvX_7ImO_G36PNLmmSIVNmkZZ4VEw3GvxRWcgY_T6lECmOGp_KkjzQKbFnaSph44ZvK_mVoCNMmDHUbU5VTJfx6hfotekw5htSjBh7CbNOwozsZl-4JFgKVzdjo1QCgFL0kfV_1YhUq1WEw6M9oEuo6LALqL2VQDVC96t0FV6q5cu87rId4afMgCQ_5RAEOKS9CMZMZkvCNA";

    @MockBean
    private AuthService service;

    @Test
    public void newEmployeeToDataBase() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", java.nio.charset.Charset.forName("UTF-8"));
        BDDMockito.given(service.newEmployeePushToDataBase("test","test","test", UserRole.ROLE_ADMIN)).willReturn(new ResponseEntity<HttpStatus>(HttpStatus.OK));
        mvc.perform(post("/api/admin/createNewEmployee").content("{\n" +
                "    \"name\": \"Kumar\",\n" +
                "    \"username\":\"yatishkumar18\",\n" +
                "    \"password\":\"testing123\",\n" +
                "    \"role\":\"ROLE_ADMIN\"\n" +
                "}").header("Authorization",this.token).accept(MEDIA_TYPE_JSON_UTF8).contentType(MEDIA_TYPE_JSON_UTF8)).andExpect(status().isOk());
    }

    @Test
    public void removeEmployeeFromDataBaseNotFound() throws Exception {
        BDDMockito.given(service.employeeRemoveFromDataBase(anyString())).willReturn(new ResponseEntity<HttpStatus>(HttpStatus.NOT_FOUND));
        mvc.perform(delete("/api/admin/removeEmployee/test").header("Authorization",this.token).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
    }

    @Test
    public void removeEmployeeFromDataBase() throws Exception {
        BDDMockito.given(service.employeeRemoveFromDataBase(anyString())).willReturn(new ResponseEntity<HttpStatus>(HttpStatus.OK));
        mvc.perform(delete("/api/admin/removeEmployee/test").header("Authorization",this.token).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }
    @Test
    public void provideAlgorithm() throws Exception {
        mvc.perform(get("/api/certificate/algorithm")).andExpect(status().isOk());
    }

    @Test
    public void getUserId() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", java.nio.charset.Charset.forName("UTF-8"));
        EmployeeCredentialsEntity employee= new EmployeeCredentialsEntity("123","test","test","test", UserRole.ROLE_ADMIN);
        BDDMockito.given(service.userDetails(anyString())).willReturn(employee);
        mvc.perform(post("/api/user_id").content("{\n" +
                "  \"username\": \"yatish.kumar\"\n" +
                "}").header("Authorization",this.token).accept(MEDIA_TYPE_JSON_UTF8).contentType(MEDIA_TYPE_JSON_UTF8)).andExpect(status().isOk()).andExpect(content().string("{\"id\":\"123\",\"role\":\"ROLE_ADMIN\"}"));

    }
}