package de.ergovia.controller;

import de.ergovia.AuthManagementMain;
import de.ergovia.entity.EmployeeCredentialsEntity;
import de.ergovia.entity.UserRole;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.*;
import static org.springframework.test.web.client.ExpectedCount.manyTimes;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@SpringBootTest(classes=AuthManagementMain.class)

@RunWith(SpringRunner.class)
public class HTTPControllerTest {

    @Autowired
    HTTPController httpController;

    @Autowired
    RestTemplate restTemplate;

    MockRestServiceServer server;

    private String url;

    @Before
    public void setUp() {
        this.server = MockRestServiceServer.createServer(restTemplate);
        this.url = "http://localhost:8081/";
    }
    @Test
    public void createUserInTimeManagement() {
        EmployeeCredentialsEntity employee= new EmployeeCredentialsEntity("123","test","test","test", UserRole.ROLE_ADMIN);
        server.expect(manyTimes(), requestTo(url +"api/createEmployee")).andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.valueOf(200)));
        ResponseEntity<HttpStatus> status = httpController.createUserInTimeManagement(employee);
        server.verify();
        Assert.assertEquals(200, status.getStatusCodeValue());
    }
}