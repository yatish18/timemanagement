package de.ergovia.security.jwtAuth;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Base64;

/**
 * Extracts private key and public key from certificate.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
public class CertificateLoader {

    private CertificateLoader() {
    }
    /**
     *loads certificate into application.
     * @param keystoreFile certificate path
     * @param password certificate password
     * @param alias certificate alias name
     * @return keyPair object.
     */
   public static KeyPairWithEncodedCert loadKeys(String keystoreFile, char[] password, String alias) {
        KeyStore keyStore;
       File file = new File(keystoreFile);
       String absolutePath = file.getAbsolutePath();
        try {
            keyStore = KeyStore.getInstance("JKS");
            keyStore.load(new FileInputStream(absolutePath), password);
            Key key = keyStore.getKey(alias, password);
            Certificate certificate = keyStore.getCertificate(alias);


            if (certificate == null) {
                throw new JWTException("Could not load Certificate");
            }

            PublicKey publicKey = certificate.getPublicKey();
            if (!"RSA".equals(publicKey.getAlgorithm())) {
                throw new JWTException("Certificate has wrong Algorithm: " + publicKey.getAlgorithm());
            }

            String encodedCert = Base64.getEncoder().encodeToString(certificate.getEncoded());

            return new KeyPairWithEncodedCert(new KeyPair(publicKey, (PrivateKey) key), encodedCert);
        } catch (KeyStoreException | IOException e) {
            throw new JWTException("Could not load KeyStore", e);
        } catch (CertificateException | NoSuchAlgorithmException | UnrecoverableKeyException e) {
            throw new JWTException("Could not load Certificate", e);
        }
    }
}
