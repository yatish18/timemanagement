package de.ergovia.security.jwtAuth;

import com.auth0.jwt.interfaces.DecodedJWT;
import de.ergovia.entity.Employee;

import java.util.Date;

/**
 * Employee object and expiretime holder once employee is validated.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
public class JWTSession {
    private Employee employee;
    private Date expireAt;

    public JWTSession(DecodedJWT decoded) {
        this.employee = new JWTUser(decoded);
        this.expireAt = decoded.getExpiresAt();
    }
    public JWTSession() {

    }

    public Employee getEmployee() {
        return employee;
    }

    public boolean isExpired(){
        return expireAt != null && expireAt.before(new Date());
    }

    public Date getExpireAt() {
        return expireAt;
    }
}
