package de.ergovia.security.jwtAuth;

import java.security.KeyPair;

/**
 * Certificate KeyPair and encoded string holder.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
public class KeyPairWithEncodedCert {
    private KeyPair keyPair;
    private String encodedCert;

    public KeyPairWithEncodedCert(KeyPair keyPair, String encodedCert) {
        this.keyPair = keyPair;
        this.encodedCert = encodedCert;
    }

    public KeyPair getKeyPair() {
        return keyPair;
    }

    public void setKeyPair(KeyPair keyPair) {
        this.keyPair = keyPair;
    }

    public String getEncodedCert() {
        return encodedCert;
    }

    public void setEncodedCert(String encodedCert) {
        this.encodedCert = encodedCert;
    }
}
