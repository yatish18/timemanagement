package de.ergovia.security.jwtAuth;

import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import de.ergovia.security.login.RestAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Loads certificate, generates tokens and validate tokens.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
@Component
public class TokenAuthenticationService implements CommandLineRunner {
    private Algorithm algorithm;
    private boolean canSign = false;
    private final long defaultExpirationSeconds=999999999;
    private String certificateData;
    private final String HEADER_STRING = "Authorization";
    private JWTVerifier verifier;
    private String keystoreFile="C:\\Program Files\\Java\\jdk1.8.0_151\\bin\\mytest.jks";
    private char[] passKey="mypass".toCharArray();
    private String alias="mytest";
    private KeyPair certificates;


    /**
     * Generate Token once user is authenticated
     * @param response attaching token in response header
     * @param user For generating token using user details
     */


    public void addAuthentication(HttpServletResponse response,JWTUser user) {
        if (!canSign) {
            throw new JWTException("Missing Private Key");
        }
        JWTCreator.Builder builder =
                com.auth0.jwt.JWT.create().withIssuer("stepnova").withClaim("username", user.getUsername())
                        .withClaim("name", user.getName())
                        .withClaim("role", user.getRole().toString());
        if (defaultExpirationSeconds!=0) {
            builder.withExpiresAt(new Date(System.currentTimeMillis() + defaultExpirationSeconds));
            response.addHeader(HEADER_STRING,builder.sign(algorithm));
        }
    }

    /**
     * loads certificate when application is started and cannot be changed
     */
    public void initCertificate(){
        KeyPairWithEncodedCert pair = CertificateLoader.loadKeys(keystoreFile, passKey, alias);
        certificateData = pair.getEncodedCert();
        certificates = pair.getKeyPair();
        canSign = certificates.getPrivate() != null;
        algorithm = Algorithm.RSA256((RSAPublicKey) certificates.getPublic(), (RSAPrivateKey) certificates.getPrivate());
        verifier = com.auth0.jwt.JWT.require(algorithm).withIssuer("stepnova").build();
    }

    public  KeyPair getCertificates() {
        return certificates;
    }

    public JWTVerifier getVerifier() {
        if (verifier == null) {
            verifier = com.auth0.jwt.JWT.require(algorithm).withIssuer("stepnova").build();
        }
        return verifier;
    }

    /**
     * Extract token from request and authenticate user accessibility
     * @param request contains already generated token
     * @return validity
     */
    public  Authentication getAuthentication(HttpServletRequest request){
        String token = request.getHeader(HEADER_STRING);
        if(token!=null) {
            try {
                DecodedJWT employee = getVerifier().verify(token);
                JWTSession session = new JWTSession(employee);
                List<GrantedAuthority> userAuthorities = new ArrayList<>();
                userAuthorities.add(new SimpleGrantedAuthority(session.getEmployee().getRole().toString()));
                return session.getEmployee() != null ? new UsernamePasswordAuthenticationToken(session.getEmployee(), null, userAuthorities) : null;
            }catch (TokenExpiredException expired){

            }
        } return null;
    }

    @Override
    public void run(String... args) throws Exception {
        this.initCertificate();
    }
}
