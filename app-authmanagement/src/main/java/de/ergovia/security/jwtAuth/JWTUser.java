package de.ergovia.security.jwtAuth;

import com.auth0.jwt.interfaces.DecodedJWT;
import de.ergovia.entity.Employee;
import de.ergovia.entity.UserRole;

/**
 * Employee details holder for JWT
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
public class JWTUser implements Employee {
    private String name;
    private String username;
    private UserRole role;

    public JWTUser(DecodedJWT decodedJWT) {
       this.name=decodedJWT.getClaim("name").asString();
       this.username= decodedJWT.getClaim("username").asString();
       this.role=decodedJWT.getClaim("role").as(UserRole.class);
    }

    public JWTUser( String name, String username, UserRole role) {
        this.name = name;
        this.username = username;
        this.role = role;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public void setRole(UserRole role) {
        this.role = role;
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public UserRole getRole() {
        return role;
    }

}

