package de.ergovia.security;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
/**
 * Authorize is employee role is admin
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */

@Service
public class AdminService {
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public boolean ensureAdmin(){
        return true;
    }
}
