package de.ergovia.security.login;

import de.ergovia.entity.EmployeeCredentialsEntity;
import de.ergovia.repo.EmployeeCredentialsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
/**
 * Employee details will be extracted from mongoDb
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */

@Service
public class EmployeeDetailsService implements UserDetailsService {
    /**
     * MongoDB object for database operations.
     */
    private EmployeeCredentialsDao credentialsDao;

    @Autowired
    public EmployeeDetailsService(EmployeeCredentialsDao credentialsDao) {
        this.credentialsDao = credentialsDao;
    }

    /**
     * Employee data is extracted from database using provided username.
     * @param username Employee username
     * @return CustomUserDetails object if found else no emplyee found.
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        EmployeeCredentialsEntity employee = credentialsDao.findAllByUsername(username);
        if(null == employee){
            throw new UsernameNotFoundException("no employee present with username:"+username);
        }
        return new CustomUserDetails(employee.getId(),employee.getName(),employee.getUsername(),employee.getPassword(),employee.getRole());
    }
}
