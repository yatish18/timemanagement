package de.ergovia.security.login;

import de.ergovia.repo.EmployeeCredentialsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Static class for getting mongoDB object
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
@Repository
public class UserLoadingDao {
    private static EmployeeCredentialsDao employeeCredentialsDao;

    public static EmployeeCredentialsDao getEmployeeCredentialsDao() {
        return employeeCredentialsDao;
    }

    @Autowired(required = true)
    public void setEmployeeCredentialsDao(EmployeeCredentialsDao employeeCredentialsDao2) {
        UserLoadingDao.employeeCredentialsDao = employeeCredentialsDao2;
    }

}
