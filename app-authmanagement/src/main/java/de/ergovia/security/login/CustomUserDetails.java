package de.ergovia.security.login;

import de.ergovia.entity.EmployeeCredentialsEntity;
import de.ergovia.entity.UserRole;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * Employee Details holder after validating.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */

public class CustomUserDetails extends EmployeeCredentialsEntity implements UserDetails {


    public CustomUserDetails(String id,String name, String username, String password, UserRole role) {
        super(id, name, username, password, role);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getUsername() {
        return super.getUsername();
    }
}
