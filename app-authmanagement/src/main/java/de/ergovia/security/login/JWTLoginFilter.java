package de.ergovia.security.login;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.ergovia.entity.Employee;
import de.ergovia.security.jwtAuth.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

/**
 * Request with /login url will be filtered
 * and this class will attempt authenticate.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */

public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {

    /**
     * TokenService object for creating token and validating token.
     */
    @Autowired
    TokenAuthenticationService tokenService;

    public JWTLoginFilter(String url, AuthenticationManager authenticationManager) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authenticationManager);
    }

    /**
     * Extract credentials from request.
     * @param request credentials
     * @param response token
     * @return is employee valid or not
     * @throws AuthenticationException
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        AccountCredentials creds = new ObjectMapper()
                .readValue(request.getInputStream(), AccountCredentials.class);
        return getAuthenticationManager().authenticate(
                new UsernamePasswordAuthenticationToken(
                        creds.getUsername(),
                        creds.getPassword(),
                        Collections.emptyList()
                )
        );

    }

    /**
     * this method calls tokenService object to create token
     * and add it to response.
     * @param request Employee Credentials.
     * @param response Token
     * @param chain
     * @param authResult employee details
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        tokenService.addAuthentication(response,this.getUserWithUsername(authResult.getName()));
    }

    /**
     * This will be called from successfulAuthentication for fetching employee from database.
     * @param userName Employee username.
     * @return return JWT user object.
     */
    public JWTUser getUserWithUsername(String userName){
        Employee employee= UserLoadingDao.getEmployeeCredentialsDao().findAllByUsername(userName);
        return new JWTUser(employee.getName(),employee.getUsername(),employee.getRole());
    }
}
