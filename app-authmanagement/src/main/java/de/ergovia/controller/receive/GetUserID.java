package de.ergovia.controller.receive;

public class GetUserID {
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
