package de.ergovia.controller.utility;

import de.ergovia.controller.send.SendNewEmployee;
import de.ergovia.entity.EmployeeCredentialsEntity;

public class ModelConverter {
    public static SendNewEmployee convertEmployeeCredentilsEntityToSendNewEmployee(EmployeeCredentialsEntity employee){
        return new SendNewEmployee(employee.getId(),employee.getUsername());
    }

}
