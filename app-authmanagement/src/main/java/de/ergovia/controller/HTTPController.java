package de.ergovia.controller;

import de.ergovia.controller.utility.ModelConverter;
import de.ergovia.entity.EmployeeCredentialsEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

/**
 * this controller can be used for communicating other applications.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
@Component
public class HTTPController implements OutGoingAPI {
    /**
     * RestTemplate bean for HTTP operations.
     */
    private RestTemplate restTemplate;
    /**
     * URL of application to reach.
     */
    private String serverURI;

    @Autowired
    public HTTPController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.serverURI = "http://localhost:8081";
    }

    public String getServerURI() {
        return serverURI;
    }

    /**
     * This is called to send employee details to create a record in time management application.
     * @param employee details like id,username,role.
     * @return status.
     */
    @Override
    public ResponseEntity createUserInTimeManagement(EmployeeCredentialsEntity employee) {
        String format = String.format("%s/api/createEmployee", this.getServerURI());
        try {
            ResponseEntity<HttpStatus> status= restTemplate.postForEntity(format, new HttpEntity<>(ModelConverter.convertEmployeeCredentilsEntityToSendNewEmployee(employee)),HttpStatus.class);
            return status;
        }catch (HttpClientErrorException err) {
            throw new HttpClientErrorAllreadyExisits(err.getStatusCode());
        }
    }
}