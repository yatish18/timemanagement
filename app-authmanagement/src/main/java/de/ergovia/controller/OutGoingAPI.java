package de.ergovia.controller;

import de.ergovia.entity.EmployeeCredentialsEntity;
import org.springframework.http.ResponseEntity;

/**
 * Interface for outgoing request.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
public interface OutGoingAPI {
    ResponseEntity createUserInTimeManagement(EmployeeCredentialsEntity employee);
}
