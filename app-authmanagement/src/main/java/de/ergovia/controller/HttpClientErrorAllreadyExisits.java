package de.ergovia.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;

/**
 * If Employee already found this class can be used.
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
@ResponseStatus(HttpStatus.CONFLICT)
public class HttpClientErrorAllreadyExisits extends HttpClientErrorException {

    public HttpClientErrorAllreadyExisits(HttpStatus statusCode) {
        super(statusCode);
    }
}
