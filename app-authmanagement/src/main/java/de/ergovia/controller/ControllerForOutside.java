package de.ergovia.controller;

import de.ergovia.controller.receive.CreateNewEmployee;
import de.ergovia.controller.receive.GetUserID;
import de.ergovia.controller.send.CertificateEncoded;
import de.ergovia.controller.send.SendNewEmployee;
import de.ergovia.controller.send.UserDetails;
import de.ergovia.controller.utility.ModelConverter;
import de.ergovia.entity.EmployeeCredentialsEntity;
import de.ergovia.security.AdminService;
import de.ergovia.security.jwtAuth.TokenAuthenticationService;
import de.ergovia.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * API's opened for Employee management operations.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
@RestController
public class ControllerForOutside {
    /**
     * can access service layer
     */
    private AuthService service;
    /**
     * can access tokenService for sending certificate.
     */
    private TokenAuthenticationService tokenAuthenticationService;
    /**
     * for validating is requesting user admin or not.
     */
    private AdminService adminService;

    @Autowired
    public ControllerForOutside(AuthService service, TokenAuthenticationService tokenAuthenticationService, AdminService adminService) {
        this.service = service;
        this.tokenAuthenticationService = tokenAuthenticationService;
        this.adminService = adminService;
    }

    /**
     * by calling this api an employee will be created
     * @param employeeDetails place holder which contains details to be created.
     * @return status
     */
    @PostMapping("/api/admin/createNewEmployee")
    public ResponseEntity<HttpStatus> newEmployeeToDataBase(@RequestBody CreateNewEmployee employeeDetails){
        adminService.ensureAdmin();
        return service.newEmployeePushToDataBase(employeeDetails.getName(),employeeDetails.getUsername(),employeeDetails.getPassword(),employeeDetails.getRole());
    }

    /**
     * by calling this api list of all employee in database
     * will be returned
     * @return employee's list
     */
    @GetMapping("/api/admin/employeeList")
    public ResponseEntity listAllEmployee(){
        adminService.ensureAdmin();
        List<EmployeeCredentialsEntity> data= service.employeeList();
        List<SendNewEmployee> employees=new ArrayList<>();
        for(EmployeeCredentialsEntity employee: data){
            employees.add(ModelConverter.convertEmployeeCredentilsEntityToSendNewEmployee(employee));
        }
        return new ResponseEntity(employees,HttpStatus.OK);
    }

    /**
     * by calling this api a employee will be removed from database.
     * @param id employee unique id.
     * @return status.
     */
    @DeleteMapping(value="/api/admin/removeEmployee/{id}")
    public ResponseEntity<HttpStatus> removeEmployeeFromDataBase(@PathVariable String id){
        adminService.ensureAdmin();
        return service.employeeRemoveFromDataBase(id);
    }

    /**
     * other service can call this api for getting certificate algorithm.
     * @return public key.
     */
    @GetMapping("/api/certificate/algorithm")
    public ResponseEntity<CertificateEncoded> provideAlgorithm(){
        return new ResponseEntity<>(new CertificateEncoded(tokenAuthenticationService.getCertificates().getPublic()),HttpStatus.OK);
    }

    /**
     * Front-end calls this api for getting employee unique id and role.
     * @param username employee username
     * @return id and role
     */
    @PostMapping("/api/user_id")
    public ResponseEntity<UserDetails> getUserId(@RequestBody GetUserID username){
        EmployeeCredentialsEntity userDetail =service.userDetails(username.getUsername());
        return new ResponseEntity<UserDetails>(new UserDetails(userDetail.getId(),userDetail.getRole().toString()),HttpStatus.OK);
    }

}
