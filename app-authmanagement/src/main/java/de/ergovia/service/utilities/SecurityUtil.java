package de.ergovia.service.utilities;

import org.springframework.security.crypto.bcrypt.BCrypt;

/**
 * Static SecurityUtil for hashing password.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */

public class SecurityUtil {
    private static int workload = 12;

    public static String generateHashFromPassword(String password){
        String salt = BCrypt.gensalt(workload);
        return BCrypt.hashpw(password, salt);
    }
}
