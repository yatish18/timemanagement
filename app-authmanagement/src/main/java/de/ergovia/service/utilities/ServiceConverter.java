package de.ergovia.service.utilities;

import de.ergovia.entity.EmployeeCredentialsEntity;
import de.ergovia.entity.UserRole;
import java.util.UUID;

/**
 * Static utility for generating id and creating employee object.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */

public class ServiceConverter {
    public static EmployeeCredentialsEntity convertFromRawDataToEmployeeCredential(String name, String username, String password, UserRole role){
        return new EmployeeCredentialsEntity(generateUUID(),name,username,password,role);
    }
    public static String generateUUID(){
        UUID uniqueKey = UUID.randomUUID();
        return uniqueKey.toString();
    }
}
