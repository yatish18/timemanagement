package de.ergovia.service;

import de.ergovia.controller.OutGoingAPI;
import de.ergovia.entity.Employee;
import de.ergovia.entity.EmployeeCredentialsEntity;
import de.ergovia.entity.UserRole;
import de.ergovia.repo.EmployeeCredentialsDao;
import de.ergovia.security.jwtAuth.JWTUser;
import de.ergovia.service.utilities.SecurityUtil;
import de.ergovia.service.utilities.ServiceConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import java.util.List;

/**
 * Service layer for creating employees
 *,removing employees, list all employees and get user id from mongoDB.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */


@Component
public class AuthService {
    /**
     * Object for accessing MongoDB operations.
     */
    private EmployeeCredentialsDao dao;
    /**
     * Object for RestTemplate operations.
     */
    private OutGoingAPI outGoingAPI;

    @Autowired
    public AuthService(EmployeeCredentialsDao dao, OutGoingAPI outGoingAPI) {
        this.dao = dao;
        this.outGoingAPI = outGoingAPI;
    }

    /**
     * By calling this method new employee will be created in database.
     *
     * @param name Employee name.
     * @param username Employee unique String ID.
     * @param password Employee Password Hasing Static method
     *                 will be called inside this method.
     * @param role Employee Role Admin or user role.
     * @return Status is it conflicting or is account created with ok status.
     */
    public ResponseEntity<HttpStatus> newEmployeePushToDataBase(String name, String username, String password, UserRole role){
        if(dao.countAllByUsername(username)>=1){
            return new ResponseEntity<HttpStatus>(HttpStatus.CONFLICT);
        }
        EmployeeCredentialsEntity employee=ServiceConverter.convertFromRawDataToEmployeeCredential(name,username, SecurityUtil.generateHashFromPassword(password),role);
        if(outGoingAPI.createUserInTimeManagement(employee).getStatusCode().equals(HttpStatus.OK))
        {
            dao.save(employee);
            return new ResponseEntity<HttpStatus>(HttpStatus.OK);
        }
        return new ResponseEntity<HttpStatus>(HttpStatus.CONFLICT);
    }

    /**
     * for getting list of all employees in database.
     * @return lists all employees.
     */
    public List<EmployeeCredentialsEntity> employeeList(){
        return dao.findAll();
    }

    /**
     * By calling this method employee will be deleted from database.
     * @param id unique id of employee
     * @return returns OK if employee is deleted successfully,
     * NOT_FOUND if employee is not presented.
     */
    public ResponseEntity<HttpStatus> employeeRemoveFromDataBase(String id) {
        if(dao.countAllById(id)>=1) {
            if(dao.findAllById(id).getId().equals(id)) {
                dao.delete(id);
                return new ResponseEntity<HttpStatus>(HttpStatus.OK);
            }
            }
        return new ResponseEntity<HttpStatus>(HttpStatus.NOT_FOUND);
    }

    /**
     * React needs userID for continuing employee operations
     * @param username Unique username for extracting userID.
     * @return User Id.
     */
    public EmployeeCredentialsEntity userDetails(String username){
        EmployeeCredentialsEntity userdata=dao.findAllByUsername(username);
        return userdata;
    }
}
