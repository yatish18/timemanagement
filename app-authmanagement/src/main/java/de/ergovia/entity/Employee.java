package de.ergovia.entity;

public interface Employee {
    public String getName();
    public String getUsername();
    public UserRole getRole();
}
