package de.ergovia.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Employee entity
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
@Document(collection="Credentials")
public class EmployeeCredentialsEntity implements Employee {
    @Id
    private String id;
    private String name;
    private String username;
    private String password;
    private UserRole role;

    public EmployeeCredentialsEntity(String id,String name, String username, String password, UserRole role) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public EmployeeCredentialsEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }
}
