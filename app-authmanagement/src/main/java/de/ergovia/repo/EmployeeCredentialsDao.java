package de.ergovia.repo;

import de.ergovia.entity.EmployeeCredentialsEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * MongoDB Repository
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
@Repository
public interface EmployeeCredentialsDao extends MongoRepository<EmployeeCredentialsEntity,String> {
     EmployeeCredentialsEntity findAllByUsername(String username);
     EmployeeCredentialsEntity findAllById(String id);
     int countAllById(String id);
     int countAllByUsername(String username);

}
