package de.ergovia.service;

import de.ergovia.entity.EmployeeEntity;
import de.ergovia.entity.TimeDetails;
import de.ergovia.entity.TimeSheetEntity;
import de.ergovia.entity.TotalWorkedByEmployee;
import de.ergovia.repo.EmployeeDao;
import de.ergovia.repo.TimeTrackerDao;
import de.ergovia.security.entity.Employee;
import de.ergovia.service.utilities.ServiceConverter;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.time.LocalDate.now;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class TrackerServiceImplTest {

    @Mock
    private EmployeeDao employeeDao;

    @Mock
    private TimeTrackerDao timeTrackerDao;

    @InjectMocks
    private TrackerServiceImpl service;

/*    @Test
    public void createEmployeeIfEmployeeAlreadyExists() {
        EmployeeEntity employee=new EmployeeEntity("123","test");
        when(employeeDao.countAllById(employee.getId())).thenReturn(1);
        ResponseEntity status= service.createEmployee(employee);
        Assert.assertSame(HttpStatus.CONFLICT,status.getStatusCode());
    }

    @Test
    public void createEmployee() {
        EmployeeEntity employee=new EmployeeEntity("123","test");
        when(employeeDao.countAllById(employee.getId())).thenReturn(0);
        ResponseEntity status= service.createEmployee(employee);
        Assert.assertSame(HttpStatus.OK,status.getStatusCode());
    }*/

  /*  @Test
    public void saveNewTimeSheetIfNoUserFound() throws ParseException {
        EmployeeEntity employee=new EmployeeEntity("123","test");
        List<TimeDetails> newSheet= new ArrayList<>();
        newSheet.add(new TimeDetails(new Date(),new Date()));
        when(employeeDao.countAllById(employee.getId())).thenReturn(0);
        String status= service.saveNewTimeSheet(employee,newSheet,new Date());
        Assert.assertSame("not found",status);

    }*/

    @Test
    public void saveNewTimeSheet() throws ParseException {
        EmployeeEntity employee=new EmployeeEntity("123","test");
        Date date=new Date();
        TimeDetails newSheet=new TimeDetails(new Date(),new Date());
        when(employeeDao.countAllByUsername(employee.getUsername())).thenReturn(1);
        when(timeTrackerDao.existsAllByEmployee_IdAndDateAndStartTimeAndEndTime(employee.getId(),date,date,date)).thenReturn(true);
        when(service.validateConflictInTimesheet(employee.getId(), date, newSheet.getStartTime(), newSheet.getEndTime())).thenReturn(false);
        ResponseEntity status= service.saveNewTimeSheet(employee,newSheet,date);
        Assert.assertSame("successful",status);

    }

    @Test
    public void calculateTotalTimePerDay() throws ParseException {
        SimpleDateFormat formating = new SimpleDateFormat("HH:mm");
        Date date=new Date();
        Date startTime=formating.parse("15:30");
        Date endTime=formating.parse("17:30");
        EmployeeEntity employee=new EmployeeEntity("123","test");
        List<TimeSheetEntity> newSheet= new ArrayList<>();
        newSheet.add(new TimeSheetEntity(employee,startTime,endTime,date));
        when(service.extractTimeForEachDate("123",date)).thenReturn(newSheet);
        long time=service.calculateTotalTimePerDay(date,"123");
        long expected=120;
        Assert.assertSame(expected,time);
    }


    @Test
    @Ignore
    public void totalWorkedFromStartOfThisMonth() {
        Date date=new Date();

        Mockito.when(service.getWorkedTimeWithDateRange("123",date,date)).thenReturn((long)120);
       // when(timeTrackerDao.existsAllByEmployee_IdAndDate("123", date)).thenReturn(true);
        long total=service.getWorkedTimeWithDateRange("123",date,date);
        long expected=120;
        Assert.assertSame(expected,total);
    }

    @Test
    public void getWorkedTimeWithDateRange() {
    }

    @Test
    public void deleteTimeSheetIfNotExists() {
        Mockito.when(timeTrackerDao.exists(123)).thenReturn(false);
        ResponseEntity status=service.deleteTimeSheet(123);
        ResponseEntity expected=new ResponseEntity(HttpStatus.NOT_FOUND);
        Assert.assertSame(expected.getStatusCode() ,status.getStatusCode());
    }

    @Test
    public void deleteTimeSheet() {
        Mockito.when(timeTrackerDao.exists(123)).thenReturn(true);
        ResponseEntity status=service.deleteTimeSheet(123);
        ResponseEntity expected=new ResponseEntity(HttpStatus.OK);
        Assert.assertSame(expected.getStatusCode() ,status.getStatusCode());
    }

    @Test
    public void getTimeSheetForOneDay() throws ParseException {
        SimpleDateFormat formating = new SimpleDateFormat("HH:mm");
        Date date=new Date();
        Date startTime=formating.parse("15:30");
        Date endTime=formating.parse("17:30");
        EmployeeEntity employee=new EmployeeEntity("123","test");
        List<TimeSheetEntity> newSheet= new ArrayList<>();
        newSheet.add(new TimeSheetEntity(employee,startTime,endTime,date));
        Mockito.when(service.extractTimeForEachDate("123",new Date())).thenReturn(newSheet);
        List<TimeSheetEntity> received=service.getTimeSheetForOneDay(date,"123");
        Assert.assertSame(endTime,received.get(0).getEndTime());

    }


    @Test
    @Ignore
    public void getAllUserWithTotalTime() {
        List<EmployeeEntity> employee=new ArrayList<>();
        employee.add(new EmployeeEntity("123","test"));
        long total=120;
        Mockito.when(employeeDao.findAll()).thenReturn(employee);
        Mockito.when(service.getWorkedTimeWithDateRange("123",new Date(),new Date())).thenReturn(total);
        List<TotalWorkedByEmployee> received= service.getAllUserWithTotalTime();
        Assert.assertSame("test",received);
    }

    @Test
    public void getAllTimeSheetByGivenDate() {

    }
}