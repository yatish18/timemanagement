package de.ergovia.service;
import de.ergovia.entity.*;
import de.ergovia.repo.EmployeeDao;
import de.ergovia.repo.TimeTrackerDao;
import de.ergovia.service.utilities.ServiceConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Service layer for timesheet operations.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
@Service
public class TrackerServiceImpl implements TrackingService {

    /**
     * Timesheet Repository bean for CRUD operations.
     */
    private TimeTrackerDao timeSheetDao;
    /**
     * Employee Repository bean for CRUD operations.
     */
    private EmployeeDao employeeDao;

    @Autowired
    public TrackerServiceImpl(TimeTrackerDao timeSheetDao, EmployeeDao employeeDao) {
        this.timeSheetDao = timeSheetDao;
        this.employeeDao = employeeDao;
    }

    /**
     * Creating employee which is called from Auth Management application,
     * employee will be created to map timeSheet.
     * @param employee Employee details for
     *                 creating record in database.
     * @return Status
     */
    @Override
    public ResponseEntity createEmployee(EmployeeEntity employee) {
        if(!(employeeDao.countAllByUsername(employee.getUsername()) >=1)) {
            employeeDao.save(employee);
            return new ResponseEntity(HttpStatus.OK);
        }
        return  new ResponseEntity(HttpStatus.CONFLICT);
    }

    /**
     * New timeSheet will be saved in database and maps it to specific employee
     * @param employeeDetails Employee information.
     * @param timeSheet Working details of employee.
     * @param date timeSheet date
     * @return status
     * @throws ParseException
     */
    @Override
    public ResponseEntity saveNewTimeSheet(EmployeeEntity employeeDetails, TimeDetails timeSheet, Date date) throws ParseException {
        String buffer="";
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        //Checks any employee is present before storing
        if(employeeDao.countAllByUsername(employeeDetails.getUsername()) <1) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        //checks is given timeSheet conflicts
        if (!validateConflictInTimesheet(employeeDetails.getId(), date, timeSheet.getStartTime(), timeSheet.getEndTime())) {
            buffer += "TimeSlot with " + format.format(timeSheet.getStartTime()) + " to " + format.format(timeSheet.getEndTime()) + " conflicts" + "\n";
        }
        //Checks given timeSheet already present in database
        if (timeSheetDao.existsAllByEmployee_IdAndDateAndStartTimeAndEndTime(employeeDetails.getId(), date, timeSheet.getStartTime(), timeSheet.getEndTime())) {
            buffer += "TimeSlot with " + format.format(timeSheet.getStartTime()) + " to " + format.format(timeSheet.getEndTime()) + " Exists" + "\n";
        }
        if (buffer.isEmpty()) {
            timeSheetDao.save(ServiceConverter.createNewTimeSheetObject(timeSheet.getStartTime(), timeSheet.getEndTime(), employeeDetails, date));
            buffer+="successful";
            TimeSheetEntity sheet=timeSheetDao.findDistinctFirstByEmployee_IdAndDateAndStartTimeAndEndTime(employeeDetails.getId(), date, timeSheet.getStartTime(), timeSheet.getEndTime());
            return new ResponseEntity<TimeSheetStatus>(new TimeSheetStatus(sheet.getId(),buffer),HttpStatus.OK);
        }
        return new ResponseEntity(buffer,HttpStatus.EXPECTATION_FAILED);
    }


    /**
     * this function will extract all timeSheet for a given day
     * and calculate total hours for that day.
     * @param searchDate date for which total hours needed.
     * @param id Employee unique id.
     * @return total working for a day.
     */
    @Override
    public long calculateTotalTimePerDay(Date searchDate,String id) {
        List<TimeSheetEntity> timingForDay=this.extractTimeForEachDate(id,searchDate);
        long totalSum=0;
        for(TimeSheetEntity eachTime: timingForDay){
          long temp=eachTime.getEndTime().getTime() - eachTime.getStartTime().getTime();
          totalSum+=temp/(60 * 1000);
        }
        return totalSum;
    }

    /**
     * When Employee submits timeSheet, a series of validation takes place
     * to make sure timeSheet does'nt conflict each other.
     * @param id Employee unique id.
     * @param date Date of timeSheet.
     * @param startTime Start of work.
     * @param endTime End of work.
     * @return true or false.
     */
    @Override
    public boolean validateConflictInTimesheet(String id, Date date, Date startTime, Date endTime) {
        //For making sure startTime is not between other TimeSlot we will get first startTime above for checking.
        TimeSheetEntity entry1=timeSheetDao.findDistinctFirstByEmployee_IdAndDateAndStartTimeBeforeOrEmployee_IdAndDateAndStartTime(id,date,startTime,id,date,startTime);
        //For making sure endTime is not between other TimeSlot we will get one endTime below for checking.
        TimeSheetEntity entry2=timeSheetDao.findDistinctFirstByEmployee_IdAndDateAndEndTimeAfterOrEmployee_IdAndDateAndEndTime(id,date,endTime,id,date,endTime);
        //Checks given startTime and endTime is not conflicting.
        if(timeSheetDao.existsAllByEmployee_IdAndDateAndStartTimeBetweenAndStartTimeAfterAndStartTimeBefore(id,date,startTime,endTime,startTime,endTime)
                || timeSheetDao.existsAllByEmployee_IdAndDateAndEndTimeBetweenAndEndTimeAfterAndEndTimeBefore(id,date,startTime,endTime,startTime,endTime)){
            return false;
        }
        //validates is startTime and endTime is between previous timeSlot.
        if(entry2!=null && entry1!=null){
            if(startTime.after(entry2.getStartTime()) && startTime.before(entry2.getEndTime())){
                return false;
            }
            if(endTime.after(entry2.getStartTime()) && endTime.before(entry2.getEndTime())){
                return false;
            }
            if(startTime.after(entry1.getStartTime()) && startTime.before(entry1.getEndTime())){
                return false;
            }
            if(endTime.after(entry1.getStartTime()) && endTime.before(entry1.getEndTime())){
                return false;
            }
        }
        return true;
    }


    /**
     * this function gets first date of current month.
     * @return first day of current month.
     */
    public Date getFirstDateOfCurrentMonth() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }

    /**
     * by calling this method total time employee worked for current month is returned .
     * @param id Employee unique id.
     * @return Total time.
     */
    @Override
    public long totalWorkedFromStartOfThisMonth(String id) {
        Date today = new Date();
        return getWorkedTimeWithDateRange(id,this.getFirstDateOfCurrentMonth(),today);
    }

    /**
     * Employee total time worked will be returned by calling this method.
     * @param employeeId Employee unique id.
     * @param fromWhen From start of date.
     * @param tillWhen Till end of date.
     * @return Total time.
     */
    @Override
    public long getWorkedTimeWithDateRange(String employeeId, Date fromWhen, Date tillWhen) {
        long totalsum=0;
        for(LocalDate date = ServiceConverter.convertDateToLocalDate(fromWhen); date.isBefore(ServiceConverter.convertDateToLocalDate(tillWhen).plusDays(1)); date=date.plusDays(1)){
            if(!timeSheetDao.existsAllByEmployee_IdAndDate(employeeId,ServiceConverter.convertLocalDateToDate(date)))
            {
                continue;
            }
            totalsum += this.calculateTotalTimePerDay(ServiceConverter.convertLocalDateToDate(date),employeeId);
        }
        return totalsum;
    }

    /**
     * for Deleting timesheet from database.
     * @param id Employee unique id.
     * @return status.
     */
    @Override
    public ResponseEntity deleteTimeSheet(int id){
        if (!timeSheetDao.exists(id)) {
            return  new ResponseEntity(HttpStatus.NOT_FOUND);

        }
        timeSheetDao.delete(id);
        return  new ResponseEntity(HttpStatus.OK);
    }

    /**
     * For displaying timesheets for a specific date  this method can be called.
     * @param searchDate date you want to get timesheets.
     * @param id employee unique id.
     * @return timesheets of given day.
     */
    @Override
    public List<TimeSheetEntity> getTimeSheetForOneDay(Date searchDate,String id){
        return this.extractTimeForEachDate(id,searchDate);
    }

    /**
     * For displaying timesheets for a specific date  this method can be called.
     * @param id employee unique id.
     * @param date date you want to get timesheets.
     * @return timesheets of given day.
     */
    @Override
    public List<TimeSheetEntity> extractTimeForEachDate(String id, Date date){
        return timeSheetDao.findAllByEmployee_IdAndDate(id,date);
    }


    /**
     * For admin to know all Employees with their total time of current month.
     * @return all Employees with total time.
     */
    public List<TotalWorkedByEmployee> getAllUserWithTotalTime(){
        Date today = new Date();
        List<EmployeeEntity> employees = employeeDao.findAll();
        List<TotalWorkedByEmployee> employeeWorkDetails=new ArrayList<>();
        for(EmployeeEntity employee : employees ){
            long total=getWorkedTimeWithDateRange(employee.getId(),this.getFirstDateOfCurrentMonth(),today);
            employeeWorkDetails.add(ServiceConverter.convertRawTimeToTotalWorkedByEmployee(total,employee.getId(),employee.getUsername()));
        }
        return employeeWorkDetails;
    }

    /**
     * for admin or employee to know previous history they can use this method by giving date range.
     * @param id Employee unique id
     * @param fromWhen date from when
     * @param tillWhen date till when
     * @return timesheets with total hours
     */
    public List<CompleteTimeSheetForGivenDate> getAllTimeSheetByGivenDate(String id, Date fromWhen, Date tillWhen){
        SimpleDateFormat formating = new SimpleDateFormat("dd-MM-yyyy");
        List<CompleteTimeSheetForGivenDate> data=new ArrayList<>();
        for(LocalDate date = ServiceConverter.convertDateToLocalDate(fromWhen); date.isBefore(ServiceConverter.convertDateToLocalDate(tillWhen).plusDays(1)); date=date.plusDays(1)){
            List<TimeSheetEntity> timeSheetForADay = getTimeSheetForOneDay(ServiceConverter.convertLocalDateToDate(date), id);
            long total=calculateTotalTimePerDay(ServiceConverter.convertLocalDateToDate(date),id);
            if(!timeSheetForADay.isEmpty()){
                data.add(new CompleteTimeSheetForGivenDate(formating.format(ServiceConverter.convertLocalDateToDate(date)),timeSheetForADay,(int)total/60,(int)total%60));
            }
        }
        return data;
    }

}
