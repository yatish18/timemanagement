package de.ergovia.service;

import de.ergovia.entity.*;
import org.springframework.http.ResponseEntity;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Service layer interface.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
public interface TrackingService
{
    ResponseEntity createEmployee(EmployeeEntity employee);

    ResponseEntity<TimeSheetStatus> saveNewTimeSheet(EmployeeEntity employeeDetails, TimeDetails timeSheet, Date date) throws ParseException;

    long getWorkedTimeWithDateRange(String employeeId, Date fromWhen, Date tillWhen);

    List<TimeSheetEntity> extractTimeForEachDate(String id, Date date);

    long calculateTotalTimePerDay(Date searchDate,String id) ;

    boolean validateConflictInTimesheet(String id, Date date, Date startTime, Date endTime);

    Date getFirstDateOfCurrentMonth();

    long totalWorkedFromStartOfThisMonth(String id);

    ResponseEntity  deleteTimeSheet(int id);

    List<TimeSheetEntity> getTimeSheetForOneDay(Date searchDate,String id);

    List<TotalWorkedByEmployee> getAllUserWithTotalTime();

    List<CompleteTimeSheetForGivenDate> getAllTimeSheetByGivenDate(String id, Date fromWhen, Date tillWhen);

}
