package de.ergovia.service.graphql;

import de.ergovia.service.graphql.datafetcher.*;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;

/**
 * GraphQL Service for Response Time measurement.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
@Service
public class TrackerServiceGraphQl {
    @Value("classpath:timesheet.graphql")
    Resource resource;
    private TotalWorkedForADayDataFetcher totalWorkedForADayDataFetcher;
    private TotalWorkedWithDateRangeDataFetcher totalWorkedWithDateRangeDataFetcher;
    private TimeSheetForADayDataFetcher timeSheetForADayDataFetcher;
    private TotalWorkedStartOfCurrentMonthDataFetcher totalWorkedStartOfCurrentMonthDataFetcher;
    private TotalWorkedTillNowByAllEmployeeDataFetcher totalWorkedTillNowByAllEmployeeDataFetcher;
    private TimeSheetForGivenDateDataFetcher timeSheetForGivenDateDataFetcher;
    private GraphQL graphQL;

    @Autowired
    public TrackerServiceGraphQl(TotalWorkedForADayDataFetcher totalWorkedForADayDataFetcher, TotalWorkedWithDateRangeDataFetcher totalWorkedWithDateRangeDataFetcher,TimeSheetForADayDataFetcher timeSheetForADayDataFetcher,TotalWorkedStartOfCurrentMonthDataFetcher totalWorkedStartOfCurrentMonthDataFetcher,TotalWorkedTillNowByAllEmployeeDataFetcher totalWorkedTillNowByAllEmployeeDataFetcher,TimeSheetForGivenDateDataFetcher timeSheetForGivenDateDataFetcher) {
        this.totalWorkedForADayDataFetcher = totalWorkedForADayDataFetcher;
        this.totalWorkedWithDateRangeDataFetcher = totalWorkedWithDateRangeDataFetcher;
        this.timeSheetForADayDataFetcher = timeSheetForADayDataFetcher;
        this.totalWorkedStartOfCurrentMonthDataFetcher = totalWorkedStartOfCurrentMonthDataFetcher;
        this.totalWorkedTillNowByAllEmployeeDataFetcher =totalWorkedTillNowByAllEmployeeDataFetcher;
        this.timeSheetForGivenDateDataFetcher = timeSheetForGivenDateDataFetcher;
    }

    @PostConstruct
    private void loadSchema() throws IOException {
        // get the schema
        File schemaFile = resource.getFile();
        // parse schema
        TypeDefinitionRegistry typeRegistry = new SchemaParser().parse(schemaFile);
        RuntimeWiring wiring = buildRuntimeWiring();
        GraphQLSchema schema = new SchemaGenerator().makeExecutableSchema(typeRegistry, wiring);
        graphQL = GraphQL.newGraphQL(schema).build();
    }

    private RuntimeWiring buildRuntimeWiring() {
        return RuntimeWiring.newRuntimeWiring()
                .type("Query", typeWiring -> typeWiring
                        .dataFetcher("totalWorkedForADay", totalWorkedForADayDataFetcher)
                        .dataFetcher("totalWorkedWithDateRange", totalWorkedWithDateRangeDataFetcher)
                        .dataFetcher("timeSheetForADay", timeSheetForADayDataFetcher)
                        .dataFetcher("totalWorkedStartOfCurrentMonth", totalWorkedStartOfCurrentMonthDataFetcher)
                        .dataFetcher("totalWorkedTillNowByAllEmployee", totalWorkedTillNowByAllEmployeeDataFetcher)
                        .dataFetcher("timeSheetForGivenDate", timeSheetForGivenDateDataFetcher))
                .build();
    }

    public GraphQL getGraphQL() {
        return graphQL;
    }
}
