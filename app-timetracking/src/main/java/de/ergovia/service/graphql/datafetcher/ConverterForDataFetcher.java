package de.ergovia.service.graphql.datafetcher;

import de.ergovia.entity.TotalWorkedByEmployee;

public class ConverterForDataFetcher {
    public static TotalWorkedByEmployee convertRawTimeToTotalWorkedByEmployee(long total,String id){
        return new TotalWorkedByEmployee(id,(int)total/60,(int)total%60);
    }
}
