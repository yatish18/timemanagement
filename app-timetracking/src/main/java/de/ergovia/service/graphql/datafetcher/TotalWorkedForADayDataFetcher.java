package de.ergovia.service.graphql.datafetcher;

import de.ergovia.entity.TotalWorkedByEmployee;
import de.ergovia.service.TrackingService;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class TotalWorkedForADayDataFetcher implements DataFetcher<TotalWorkedByEmployee> {
    private TrackingService trackingService;
    private SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
    @Autowired
    public TotalWorkedForADayDataFetcher(TrackingService trackingService) {
        this.trackingService = trackingService;
    }

    @Override
    public TotalWorkedByEmployee get(DataFetchingEnvironment environment) {
        String id = environment.getArgument("id");
        Date date=null;
        try {
            date = this.format.parse(environment.getArgument("date"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ConverterForDataFetcher.convertRawTimeToTotalWorkedByEmployee(trackingService.calculateTotalTimePerDay(date,id),id);
    }
}
