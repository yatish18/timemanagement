package de.ergovia.service.graphql.datafetcher;

import de.ergovia.entity.TotalWorkedByEmployee;
import de.ergovia.service.TrackingService;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TotalWorkedStartOfCurrentMonthDataFetcher implements DataFetcher<TotalWorkedByEmployee> {

    private TrackingService trackingService;

    @Autowired
    public TotalWorkedStartOfCurrentMonthDataFetcher(TrackingService trackingService) {
        this.trackingService = trackingService;
    }

    @Override
    public TotalWorkedByEmployee get(DataFetchingEnvironment environment) {
        String id = environment.getArgument("id");
        return ConverterForDataFetcher.convertRawTimeToTotalWorkedByEmployee(trackingService.totalWorkedFromStartOfThisMonth(id),id);
    }
}
