package de.ergovia.service.graphql.datafetcher;

import de.ergovia.entity.CompleteTimeSheetForGivenDate;
import de.ergovia.service.TrackingService;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class TimeSheetForGivenDateDataFetcher implements DataFetcher<List<CompleteTimeSheetForGivenDate>> {

    private TrackingService trackingService;
    private SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
    @Autowired
    public TimeSheetForGivenDateDataFetcher(TrackingService trackingService) {
        this.trackingService = trackingService;
    }

    @Override
    public List<CompleteTimeSheetForGivenDate> get(DataFetchingEnvironment environment) {
        String id = environment.getArgument("id");
        Date fromWhen=null;
        Date tillWhen=null;
        try {
            fromWhen = this.format.parse(environment.getArgument("fromWhen"));
            tillWhen = this.format.parse(environment.getArgument("tillWhen"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return trackingService.getAllTimeSheetByGivenDate(id,fromWhen,tillWhen);
    }
}
