package de.ergovia.service.graphql.datafetcher;

import de.ergovia.entity.TimeSheetEntity;
import de.ergovia.service.TrackingService;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class TimeSheetForADayDataFetcher implements DataFetcher<List<TimeSheetEntity>>{

    private TrackingService trackingService;
    private SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

    @Autowired
    public TimeSheetForADayDataFetcher(TrackingService trackingService) {
        this.trackingService = trackingService;
    }

    @Override
    public List<TimeSheetEntity> get(DataFetchingEnvironment environment) {
        String id = environment.getArgument("id");
        Date date=null;
        try {
            date = this.format.parse(environment.getArgument("date"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return trackingService.getTimeSheetForOneDay(date,id);
    }
}
