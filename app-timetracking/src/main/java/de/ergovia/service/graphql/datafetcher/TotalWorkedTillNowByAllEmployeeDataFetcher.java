package de.ergovia.service.graphql.datafetcher;

import de.ergovia.entity.TotalWorkedByEmployee;
import de.ergovia.service.TrackingService;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TotalWorkedTillNowByAllEmployeeDataFetcher implements DataFetcher<List<TotalWorkedByEmployee>> {
    private TrackingService trackingService;

    @Autowired
    public TotalWorkedTillNowByAllEmployeeDataFetcher(TrackingService trackingService) {
        this.trackingService = trackingService;
    }

    @Override
    public List<TotalWorkedByEmployee> get(DataFetchingEnvironment environment) {
        return trackingService.getAllUserWithTotalTime();
    }
}
