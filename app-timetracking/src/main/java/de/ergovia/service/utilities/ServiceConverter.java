package de.ergovia.service.utilities;

import de.ergovia.controller.model.send.PostWeekWorkedRequest;
import de.ergovia.entity.EmployeeEntity;
import de.ergovia.entity.TimeSheetEntity;
import de.ergovia.entity.TotalWorkedByEmployee;

import java.time.*;
import java.util.Date;

/**
 * Converter for TrackerService.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
public class ServiceConverter {
    public static TimeSheetEntity createNewTimeSheetObject(Date startTime, Date endTime, EmployeeEntity employee, Date date){
        return new TimeSheetEntity(employee,startTime,endTime,date);
    }

    public static LocalTime convertDateToLocalTime(Date date){
        Instant instant = Instant.ofEpochMilli(date.getTime());
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalTime();
    }

    public static LocalDate convertDateToLocalDate(Date date){
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static Date convertLocalDateToDate(LocalDate localDate){
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }
    public static TotalWorkedByEmployee convertRawTimeToTotalWorkedByEmployee(long time, String id,String username){
        return new TotalWorkedByEmployee(id,username,(int)time/60,(int)time%60);
    }

}
