package de.ergovia.entity;


import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;

/**
 * Entity for Employees
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
@Entity
@Table(name = "employee")
public class EmployeeEntity {


    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "username")
    private String username;
    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<TimeSheetEntity> timeSheets;


    public EmployeeEntity(String id, String username) {
        this.id = id;
        this.username = username;
    }

    public EmployeeEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setName(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "EmployeeEntity{" +
                "id=" + id +
                ", name='" + username + '\'' +
                ", products=" + timeSheets +
                '}';
    }
}
