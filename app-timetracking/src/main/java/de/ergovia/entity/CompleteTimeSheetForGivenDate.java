package de.ergovia.entity;

import de.ergovia.entity.TimeSheetEntity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * For admin to get complete details about timesheets, hours, minutes and date
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
public class CompleteTimeSheetForGivenDate {
    private String date;
    private List<TimeSheetEntity> timeSheet;
    private int hours;
    private int minutes;
    private SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

    public CompleteTimeSheetForGivenDate(String date, List<TimeSheetEntity> timeSheet, int hours, int minutes) {
        this.date = date;
        this.timeSheet = timeSheet;
        this.hours = hours;
        this.minutes = minutes;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public String getDate() throws ParseException {
        return date;
    }

    public void setDate(String date) throws ParseException {
        this.date = date;

    }

    public List<TimeSheetEntity> getTimeSheet() {
        return timeSheet;
    }

    public void setTimeSheet(List<TimeSheetEntity> timeSheet) {
        this.timeSheet = timeSheet;
    }
}
