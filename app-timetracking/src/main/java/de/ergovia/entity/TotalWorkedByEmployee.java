package de.ergovia.entity;

/**
 * Employee with their working hours
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
public class TotalWorkedByEmployee {
    private String employeeId;
    private String username;
    private int hours;
    private int minute;


    public TotalWorkedByEmployee(String employeeId, String username, int hours, int minute) {
        this.employeeId = employeeId;
        this.username = username;
        this.hours = hours;
        this.minute = minute;
    }

    public TotalWorkedByEmployee(String employeeId, int hours, int minute) {
        this.employeeId = employeeId;
        this.hours = hours;
        this.minute = minute;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }
}
