package de.ergovia.entity;


import javax.persistence.*;

import java.util.Date;

/**
 * TimeSheet Entity
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
@Entity
@Table(name="timesheet")
public class TimeSheetEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "employee_id")
    private EmployeeEntity employee;
    @Temporal(TemporalType.DATE)
    private Date date;
    @Column(name = "start_Time")
    @Temporal(TemporalType.TIME)
    private Date startTime;
    @Temporal(TemporalType.TIME)
    @Column(name = "end_Time")
    private Date endTime;

    private transient  String formattedDate;
    private transient  String formattedStartTime;
    private transient  String formattedEndTime;


    public TimeSheetEntity(EmployeeEntity employee, Date startTime, Date endTime,Date date) {
        this.employee = employee;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public TimeSheetEntity() {
    }

    public String getFormattedDate() {
        return getDate().toString();
    }

    public String getFormattedStartTime() {
        return getStartTime().toString();
    }

    public String getFormattedEndTime() {
        return getEndTime().toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EmployeeEntity getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeEntity employee) {
        this.employee = employee;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
