package de.ergovia.repo;

import de.ergovia.entity.EmployeeEntity;
import de.ergovia.entity.TimeSheetEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 * MongoDB Repository for timesheet
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
public interface TimeTrackerDao extends JpaRepository<TimeSheetEntity, Integer> {

 boolean existsAllByEmployee_IdAndDateAndStartTimeBetweenAndStartTimeAfterAndStartTimeBefore(String id,Date date,Date sStart,Date sEnd,Date afterStart,Date beforeEnd);

 boolean existsAllByEmployee_IdAndDateAndEndTimeBetweenAndEndTimeAfterAndEndTimeBefore(String id,Date date,Date eStart,Date eEnd,Date afterStart,Date beforeEnd);

 boolean existsAllByEmployee_IdAndDateAndStartTimeAndEndTime(String id,Date date,Date startTime,Date endTime);

 boolean existsAllByEmployee_IdAndDate(String id,Date date);

 TimeSheetEntity findDistinctFirstByEmployee_IdAndDateAndStartTimeBeforeOrEmployee_IdAndDateAndStartTime(String id1,Date date1,Date startTimeBefore,String id2,Date date2,Date startTime);
 TimeSheetEntity findDistinctFirstByEmployee_IdAndDateAndEndTimeAfterOrEmployee_IdAndDateAndEndTime(String id1,Date date1,Date endTimeBefore,String id2,Date date2,Date endTime);

 List<TimeSheetEntity> findAllByEmployee_IdAndDate(String id,Date date);

 TimeSheetEntity findDistinctFirstByEmployee_IdAndDateAndStartTimeAndEndTime(String id,Date date,Date startTime,Date endTime);


 @Transactional
 @Modifying
 @Query("delete from TimeSheetEntity t where t.id = ?1")
 void delete(int id);
}
