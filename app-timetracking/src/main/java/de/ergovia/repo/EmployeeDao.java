package de.ergovia.repo;

import de.ergovia.entity.EmployeeEntity;
import de.ergovia.entity.TimeSheetEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * MongoDB Repository for employee
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
public interface EmployeeDao extends JpaRepository<EmployeeEntity, String> {
    int countAllByUsername(String username);
    List<EmployeeEntity> findAll();
}
