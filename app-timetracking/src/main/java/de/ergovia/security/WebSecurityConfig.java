package de.ergovia.security;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Configuration;

import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


/**
 * Security configuration for API's
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * Object for responding with UNAUTHORIZED if authentication failed.
     */
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;
    /**
     * Object for handling JWT authentication.
     */
    private JWTAuthenticationFilter jwtAuthenticationFilter;

    @Autowired
    public WebSecurityConfig(JWTAuthenticationFilter jwtAuthenticationFilter,RestAuthenticationEntryPoint restAuthenticationEntryPoint) {
        this.jwtAuthenticationFilter = jwtAuthenticationFilter;
        this.restAuthenticationEntryPoint = restAuthenticationEntryPoint;
    }

    /**
     * Specifies what should be open and what should be authenticated.
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .exceptionHandling()
                .authenticationEntryPoint(restAuthenticationEntryPoint)
                .and()
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(jwtAuthenticationFilter,UsernamePasswordAuthenticationFilter.class);

    }

    /**
     * Ignoring security authentication for asking certificate
     * using provided API.
     * @param web
     * @throws Exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/api/createEmployee")/*.antMatchers("/graphql/timetracking/{query}").antMatchers("/api/admin/totalWorkedTillNowByAllEmployee").antMatchers("/totalWorkForCurrentMonth/{id}").antMatchers("/totalWorkedWithDateRange/{id}/{fromWhen}/{tillWhen}").antMatchers("/api/timesheetWithRange/{id}/{fromWhen}/{tillWhen}")*/;
    }

}
