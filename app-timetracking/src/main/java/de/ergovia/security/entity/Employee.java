package de.ergovia.security.entity;

public interface Employee {
    public String getName();
    public String getUsername();
    public UserRole getRole();
}
