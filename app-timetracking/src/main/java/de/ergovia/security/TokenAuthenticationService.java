package de.ergovia.security;


import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import de.ergovia.controller.ControllerTimeSheet;
import de.ergovia.controller.OutGoingAPI;
import de.ergovia.controller.model.send.CertificateEncoded;
import de.ergovia.security.entity.JWTSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import com.auth0.jwt.algorithms.Algorithm;
import javax.servlet.http.HttpServletRequest;
import java.security.*;

import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Loads certificate and validate tokens.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
@Component
public class TokenAuthenticationService{

    static Algorithm algorithm;
    static boolean canValidate = false;
    static final String HEADER_STRING = "Authorization";
    static JWTVerifier verifier;
    private OutGoingAPI restAPI;
    static CertificateEncoded keys;

    @Autowired
    public TokenAuthenticationService(OutGoingAPI restAPI) {
        this.restAPI = restAPI;
    }

    /**
     * loads certificate when application is started and cannot be changed
     */
    public  void initializeAlgorithm() throws InvalidKeySpecException, NoSuchAlgorithmException {
        keys=restAPI.getCetificate();
        PublicKey publicKey =
                KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(keys.getPublicKey()));
        algorithm = Algorithm.RSA256((RSAPublicKey) publicKey,null );
    }

    public static JWTVerifier getVerifier() {
        if (verifier == null) {
            verifier = com.auth0.jwt.JWT.require(algorithm).withIssuer("stepnova").build();
        }
        return verifier;
    }

    /**
     * Extract token from request and authenticate user accessibility
     * @param request contains already generated token
     * @return validity
     */
    public Authentication getAuthentication(HttpServletRequest request) throws InvalidKeySpecException, NoSuchAlgorithmException {
        String token = request.getHeader(HEADER_STRING);
        if(token!=null) {
            if(!canValidate){
                initializeAlgorithm();
                canValidate = true;
            }
            try{
                DecodedJWT employee = getVerifier().verify(token);
                JWTSession session = new JWTSession(employee);
                List<GrantedAuthority> authorities = new ArrayList<>(); ;
                authorities.add(new SimpleGrantedAuthority(session.getEmployee().getRole().toString()));
                return session.getEmployee() != null ? new UsernamePasswordAuthenticationToken(session.getEmployee(), null,authorities) : null;
            }
            catch (TokenExpiredException msg){
//Todo
            }


        } return null;
    }
}
