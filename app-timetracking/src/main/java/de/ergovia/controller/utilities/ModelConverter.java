package de.ergovia.controller.utilities;

import de.ergovia.controller.model.receive.CreateEmployee;
import de.ergovia.controller.model.receive.TimeDetailsReceived;
import de.ergovia.controller.model.receive.EmployeeTimeSheetForm;
import de.ergovia.controller.model.send.PostWeekWorkedRequest;
import de.ergovia.entity.EmployeeEntity;
import de.ergovia.entity.TimeDetails;
import de.ergovia.entity.TotalWorkedByEmployee;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class ModelConverter {
    public static EmployeeEntity convertEmployeeTimeSheetToEmployeeEntity(EmployeeTimeSheetForm form){
        return new EmployeeEntity(form.getId(),form.getUsername());
    }
    public static TimeDetails convertReceivedTimeDetailsToServiceTimeDetails(TimeDetailsReceived details){
        return new TimeDetails(details.getStartTime(),details.getEndTime());
    }

    public static EmployeeEntity convertCreateEmployeeToEmployeeEntity(CreateEmployee employee){
        return new EmployeeEntity(employee.getId(),employee.getUsername());
    }

/*    public static TimeSheetEntity convertEmployeeTimeSheetToTimeSheetEntity(EmployeeTimeSheetForm form){
        return new TimeSheetEntity(ModelConverter.convertEmployeeTimeSheetToEmployeeEntity(form),form.getWorkLoad().getTimeDetails());
    }*/
    public static TotalWorkedByEmployee convertRawTimeToTotalWorkedRequest(long time,String id){
        return new TotalWorkedByEmployee(id,(int)time/60,(int)time%60);
    }
}
