package de.ergovia.controller;


import de.ergovia.controller.model.send.CertificateEncoded;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * RestTemplate Controller for Getting certificate from Auth Management.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
@Component
public class HTTPController implements OutGoingAPI{
    private RestTemplate restTemplate;
    private String serverURI;

    @Autowired
    public HTTPController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.serverURI = "http://localhost:8083";
    }

    public String getServerURI() {
        return serverURI;
    }

    @Override
    public CertificateEncoded getCetificate() {
        String format = String.format("%s/api/certificate/algorithm", this.getServerURI());
        ResponseEntity<CertificateEncoded> algorithm= restTemplate.getForEntity(format,CertificateEncoded.class);
        System.out.println(algorithm.getBody());
        return algorithm.getBody();
    }
}
