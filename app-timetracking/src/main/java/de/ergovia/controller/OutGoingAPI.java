package de.ergovia.controller;

import de.ergovia.controller.model.send.CertificateEncoded;

/**
 * Interface for HTTPController.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
public interface OutGoingAPI {
    CertificateEncoded getCetificate();
}
