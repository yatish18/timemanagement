package de.ergovia.controller;

import de.ergovia.service.graphql.TrackerServiceGraphQl;
import graphql.ExecutionResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller for GraphQL measurement.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
@RestController
public class GraphQLController {

    private TrackerServiceGraphQl serviceGraphQl;

    @Autowired
    public GraphQLController(TrackerServiceGraphQl serviceGraphQl) {
        this.serviceGraphQl = serviceGraphQl;
    }

    @GetMapping("/graphql/timetracking/{query}")
    public ResponseEntity<Object> getAllTimeDetails(@PathVariable String query){
        ExecutionResult executionResult = serviceGraphQl.getGraphQL().execute(query);
        return new ResponseEntity<>(executionResult, HttpStatus.OK);
    }
}