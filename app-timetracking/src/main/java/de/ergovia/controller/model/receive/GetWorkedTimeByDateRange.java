package de.ergovia.controller.model.receive;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GetWorkedTimeByDateRange {
    private Date fromWhen;
    private Date tillWhen;
    private int employeeId;
    private SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

    public Date getFromWhen() {
        return fromWhen;
    }

    public void setFromWhen(String fromWhen) throws ParseException {
        this.fromWhen = this.format.parse(fromWhen);
    }

    public Date getTillWhen() {
        return tillWhen;
    }

    public void setTillWhen(String tillWhen) throws ParseException {
        this.tillWhen = this.format.parse(tillWhen);
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public String toString() {
        return "GetWorkedTimeByDateRange{" +
                "fromWhen=" + fromWhen +
                ", tillWhen=" + tillWhen +
                ", employeeId=" + employeeId +
                '}';
    }
}
