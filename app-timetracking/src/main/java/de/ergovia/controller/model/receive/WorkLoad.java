package de.ergovia.controller.model.receive;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class WorkLoad {
    private Date date;
    private TimeDetailsReceived timeDetails;

    public WorkLoad() {
    }

    public WorkLoad(Date date, TimeDetailsReceived timeDetails) {
        this.date = date;
        this.timeDetails = timeDetails;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(String date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        this.date = format.parse(date);
    }

    public TimeDetailsReceived getTimeDetails() {
        return timeDetails;
    }

    public void setTimeDetails(TimeDetailsReceived timeDetails) {
        this.timeDetails = timeDetails;
    }
}
