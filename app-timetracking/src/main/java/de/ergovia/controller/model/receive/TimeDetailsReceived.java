package de.ergovia.controller.model.receive;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeDetailsReceived {
    private Date startTime;
    private Date endTime;


    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        this.startTime =  format.parse(startTime);
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        this.endTime =  format.parse(endTime);
    }
}
