package de.ergovia.controller.model.send;

import java.util.Date;

public class PostWeekWorkedRequest {
    private String employeeId;
    private int hours;
    private int minute;


    public PostWeekWorkedRequest(String employeeId, int hours, int minute) {
        this.employeeId = employeeId;
        this.hours = hours;
        this.minute = minute;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

}
