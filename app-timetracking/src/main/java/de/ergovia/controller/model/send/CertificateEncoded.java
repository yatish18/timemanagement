package de.ergovia.controller.model.send;
import java.security.PrivateKey;
import java.security.PublicKey;

public class CertificateEncoded {
    private byte[] publicKey;

    public CertificateEncoded(PublicKey publicKey) {
        this.publicKey = publicKey.getEncoded();

    }

    public CertificateEncoded() {
    }

    public byte[] getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(byte[] publicKey) {
        this.publicKey = publicKey;
    }
}
