package de.ergovia.controller;

import de.ergovia.controller.model.receive.CreateEmployee;
import de.ergovia.controller.model.receive.EmployeeTimeSheetForm;
import de.ergovia.entity.CompleteTimeSheetForGivenDate;
import de.ergovia.controller.utilities.ModelConverter;
import de.ergovia.entity.TimeSheetEntity;
import de.ergovia.entity.TotalWorkedByEmployee;
import de.ergovia.security.AdminService;
import de.ergovia.service.TrackingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Controller for outside access
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
@RestController
public class ControllerTimeSheet {
    /**
     * can access service layer
     */
    private TrackingService service;
    /**
     * for validating is requesting user admin or not.
     */
    private AdminService adminService;

    @Autowired
    public ControllerTimeSheet(TrackingService service, AdminService adminService) {
        this.service = service;
        this.adminService = adminService;
    }


    /**
     * Creating new timesheet front end will call this api with timesheet details.
     * @param form Timesheet details
     * @return Status.
     * @throws ParseException
     */
    @PostMapping("/newSheet")
    public ResponseEntity saveNewTimeSheet(@RequestBody EmployeeTimeSheetForm form) throws ParseException {
        return service.saveNewTimeSheet(ModelConverter.convertEmployeeTimeSheetToEmployeeEntity(form), ModelConverter.convertReceivedTimeDetailsToServiceTimeDetails(form.getWorkLoad().getTimeDetails()),form.getWorkLoad().getDate());
    }

    /**
     * Total hours worked by employee per day can be extracted here
     * @param id Employee unique id
     * @param date on which date total hours worked is needed
     * @return Total time
     */
    @GetMapping("/totalHoursPerDay/{id}/{date}")
    public ResponseEntity totalHoursPerDay(@PathVariable String id,@DateTimeFormat(pattern = "dd-MM-yyyy") @PathVariable("date") Date date) {
        long total=service.calculateTotalTimePerDay(date,id);
        return new ResponseEntity(ModelConverter.convertRawTimeToTotalWorkedRequest(total,id),HttpStatus.OK);
    }


    /**
     * Total hours of employee with date range.
     * @param id Employee unique id.
     * @param fromWhen From when date.
     * @param tillWhen Till when date.
     * @return Total Time.
     */
    @GetMapping("/totalWorkedWithDateRange/{id}/{fromWhen}/{tillWhen}")
    public ResponseEntity totalHoursWithDate(@PathVariable String id,@DateTimeFormat(pattern = "dd-MM-yyyy") @PathVariable("fromWhen") Date fromWhen,@DateTimeFormat(pattern = "dd-MM-yyyy") @PathVariable("tillWhen") Date tillWhen){
      long rawTotalHours=service.getWorkedTimeWithDateRange(id,fromWhen,tillWhen);
      if(rawTotalHours == 0){
          return new ResponseEntity(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity(ModelConverter.convertRawTimeToTotalWorkedRequest(rawTotalHours,id),HttpStatus.OK);
    }

    /**
     * Employees total time worked for current month.
     * @param id Employee Unique id.
     * @return Total time for month.
     */
    @GetMapping("/totalWorkForCurrentMonth/{id}")
    public ResponseEntity totalWorkForCurrentMonth(@PathVariable String id){
        long rawTotalHours = service.totalWorkedFromStartOfThisMonth(id);
        return new ResponseEntity(ModelConverter.convertRawTimeToTotalWorkedRequest(rawTotalHours,id),HttpStatus.OK);
    }


    /**
     * Create new employee in database.
     * @param employee employee details.
     * @return status
     */
    @PostMapping("/api/createEmployee")
    public ResponseEntity createEmployee(@RequestBody CreateEmployee employee){
        return service.createEmployee(ModelConverter.convertCreateEmployeeToEmployeeEntity(employee));
    }


    /**
     * Delete timesheet from database.
     * @param id Employee unique id.
     * @return status.
     */
    @DeleteMapping("/api/deleteTimesheet/{id}")
    public ResponseEntity deleteTimesheet(@PathVariable int id){
        return service.deleteTimeSheet(id);
    }

    /**
     * Employee Timesheets for a day can be retrieved by calling this API.
     * @param id Employee Unique id.
     * @param date Date for fetching specific data.
     * @return Tmesheets of given date.
     */
    @GetMapping("/api/timesheetForOneDay/{id}/{date}")
    public List<TimeSheetEntity> timeSheetForOneDay(@PathVariable String id,@DateTimeFormat(pattern = "dd-MM-yyyy") @PathVariable("date") Date date){
        return service.getTimeSheetForOneDay(date,id);
    }

    /**
     * List of all employees with total time worked.
     * @return list of employees with total worked time.
     */
    @GetMapping("/api/admin/totalWorkedTillNowByAllEmployee")
    public List<TotalWorkedByEmployee> employeelist(){
        adminService.ensureAdmin();
        return service.getAllUserWithTotalTime();
    }

    /**
     * Employee timeesheets for given date range
     * @param id Employee unique id.
     * @param fromWhen From when date.
     * @param tillWhen Till when date.
     * @return TimeSheets details.
     */
    @GetMapping("/api/timesheetWithRange/{id}/{fromWhen}/{tillWhen}")
    public List<CompleteTimeSheetForGivenDate> timeSheetForGivenDate(@PathVariable String id,@DateTimeFormat(pattern = "dd-MM-yyyy") @PathVariable("fromWhen") Date fromWhen,@DateTimeFormat(pattern = "dd-MM-yyyy") @PathVariable("tillWhen") Date tillWhen){
        return service.getAllTimeSheetByGivenDate(id,fromWhen,tillWhen);
    }


}
