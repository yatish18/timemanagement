import React from 'react'
import ReactDom from 'react-dom'
import {Provider} from 'react-redux'
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import store, {history} from './store/configureStore';
import routes from './routes';
import {Router} from 'react-router'





ReactDom.render(
    <Provider store={store}>
        <Router history={history} routes={routes}>
        </Router>
    </Provider>,
    document.getElementById("root")
);
