import axios from "axios/index";
import {authentication} from "../reducers/authentication.reducer";
import {browserHistory} from "react-router";


let dashBoardRender={
    totalWorkedTodayObject:{
        hours:"",
        minute:""
    },totalWorkedCurrentMonth:{
        hours:"",
        minute:""
    },timeSheetForToday:[]
}
let timeSheet={
    id:"",
    username:"",
    workLoad:{
        date:"",
        timeDetails:[]
    }
}
let workHours={
    startTime:"",
    endTime:""
}

export function loadDashboard(user) {
    var date=new Date()
    var yyyy=date.getFullYear();
    var dd=date.getDate();
    var mm=date.getMonth()+1;
    var formattedDate=dd+"-"+mm+"-"+yyyy;
    return dispatch => {
        if(user.validity===false){
            browserHistory.push('/');
        }
        else{
            axios({
                method: 'GET',
                url: 'http://localhost:8080/getTotalWorked/'+user.id,
                headers:{'Authorization': user.token}
            }).then(userdata=>{
                dashBoardRender.totalWorkedCurrentMonth.hours=userdata.data.hours;
                dashBoardRender.totalWorkedCurrentMonth.minute=userdata.data.minute;
                axios({
                    method: 'GET',
                    url: 'http://localhost:8080/getTotalWorkedForToday/'+user.id+'/'+formattedDate,
                    headers:{'Authorization': user.token}}).then(userdata=>{
                    dashBoardRender.totalWorkedTodayObject.hours=userdata.data.hours;
                    dashBoardRender.totalWorkedTodayObject.minute=userdata.data.minute;
                    axios({
                        method: 'GET',
                        url: 'http://localhost:8080/timesheetForOneDay/'+user.id+'/'+formattedDate,
                        headers:{'Authorization': user.token}}).then(userdata=>{
                        dashBoardRender.timeSheetForToday=userdata.data;
                        dispatch(totalWorked(dashBoardRender));
                    })
                })
            })
        }

    }
}

export function deleteTimeSlot(id,user) {
    return dispatch => {
        axios({
            method: 'DELETE',
            url: 'http://localhost:8080/api/deleteTimesheet/'+id,
            headers:{'Authorization': user.token}
        }).then(userdata=>{
            if(userdata.status!==200){
                alert("Remove failed");
            }else{
                dispatch(deleteAction(id));
            }

        })
    }
}

export function addNewTimeSheet(sheet,user) {
    console.log(sheet.date)
    axios.post('http://localhost:8080/submitNewSheet', {
        "id": user.id,
        "username": user.username,
        "workLoad": {
            "date":sheet.date,
            "timeDetails":
                {
                    "startTime": sheet.startTime,
                    "endTime": sheet.endTime
                }
        }
    }, {
        headers: {'Authorization': user.token}
    }).then(data=>{
            console.log(data)
    })
}

function deleteAction(id){
    const action={
        type:"DELETE_TIMESLOT",
        id
    }
    return action;
}

function totalWorked(workedMonthly) { return { type: "TOTAL_WORKED", workedMonthly } }

//function totalWorked(worked) { return { type: "TOTAL_WORKED", worked } }

