import React,{Component} from "react"
import TotalWorked from "./container/DisplayStatus"
import { connect } from "react-redux"
import AddTimeSheet from "./container/AddTimeSheet"
import "./Dashboard.css"
import { logout} from "../../action/login_actions";
import {deleteTimeSlot, loadDashboard} from "../../action/user_actions";
import {browserHistory} from "react-router";


class Dashboard extends Component {

    signout(){
        this.props.logout();
    }

    render() {

        return (
            <div>
                <div >
                    <div className={"btn btn-danger userSignout"} onClick={()=>this.signout()}>Sign out</div>
                    <div className={"btn btn-primary history"} onClick={()=>browserHistory.push('/User_dashboard/History')}>History</div>
                </div>
                <h1 className={"userdashboard"}>DashBoard</h1>
                <div className={"usercontainer"}>
                    <div className={"timeSheet"}><TotalWorked /></div>
                    <div className={"addSheet"}><AddTimeSheet/></div>
                </div>
            </div>

        );
    }
}

const mapStateToProps = ({authentication, user_dashboard}) => ({authentication, user_dashboard});

const mapDispatchToProps = {
    logout
};

export default connect(mapStateToProps,mapDispatchToProps)(Dashboard)