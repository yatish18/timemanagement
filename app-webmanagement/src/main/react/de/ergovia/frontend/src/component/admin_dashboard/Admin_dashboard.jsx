import React,{Component} from "react"
import {logout} from "../../action/login_actions";
import {connect} from "react-redux";
import DisplayEmployee from "./DisplayEmployee"
import "./AdminDashBoard.css"
import {browserHistory} from "react-router";

class Admin_dashboard extends Component {

    signout(){
        this.props.logout();
    }

    render() {

        return (
            <div>
                <div ><h1 className={"Admindashboard"}>DashBoard</h1>
                    <div className={"btn btn-danger signout"} onClick={()=>this.signout()}>Sign out</div>
                    <button className={"btn btn-primary history"}onClick={ ()=>browserHistory.push('/Admin_dashboard/History')}>History</button>
                    <button className={"btn btn-primary history"}onClick={ ()=>browserHistory.push('/Admin_dashboard/Users')}>Users</button>
                </div>
            <DisplayEmployee/>
            </div>

        );
    }
}
const mapStateToProps = ({authentication}) => ({authentication});

const mapDispatchToProps = {
    logout
};


export default connect(mapStateToProps,mapDispatchToProps)(Admin_dashboard)