import { userConstants } from '../constants/user.constants';
import {bake_cookie,read_cookie} from "sfcookies"
import {adminBoard} from "./admin_dashboard";

let user = {
    id:"",
    username: "",
    role:"",
    token:"",
    tokenValidity: false
}


const initialState = user.tokenvalidity ? { user } : {};

export function authentication(state = initialState, action) {
    let authenticated =null;
    let clear={};
    let listClear=[];
    state =read_cookie("Authentication")
    switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        loggingIn: true,
        user: action.sendUser
      };
    case userConstants.LOGIN_SUCCESS:
        authenticated=action.user
            bake_cookie("Authentication",authenticated)
        return Object.assign({},action.user);
      case userConstants.LOGIN_FAILURE:
        alert("Invalid User");
          return {};
        case userConstants.LOGOUT:
          authenticated={validity:false};
          bake_cookie("Authentication",authenticated)
          return authenticated;
    default:
      return state
  }
}