import {bake_cookie,read_cookie} from "sfcookies"


const refreshDeletedItem=(state,id)=>{
    state.timeSheetForToday=state.timeSheetForToday.filter(sheet=> sheet.id!==id)
    return state;
}
const refreshAddItem=(state,id)=>{
    state.timeSheetForToday=state.timeSheetForToday.filter(sheet=> sheet.id!==id)
    return state;
}

export  function user_dashboard(state = {}, action) {
    state=read_cookie("TodaysTimeSheet");
    switch(action.type) {
        case 'TOTAL_WORKED':
            bake_cookie("TodaysTimeSheet",action.workedMonthly)
            return Object.assign({},action.workedMonthly);
        case 'DELETE_TIMESLOT':
            bake_cookie("TodaysTimeSheet",refreshDeletedItem(state,action.id))
            return refreshDeletedItem(state,action.id);
        case 'ADD_TIMESHEET':
            return Object.assign({},action);
        default:
            return state;
    }
}