import axios from "axios/index";
import {authentication} from "../reducers/authentication.reducer";
import {userConstants} from "../constants/user.constants";
import {browserHistory} from "react-router";

let employeeData=[];

export function loadAdminDashboard(admin) {
    return dispatch => {
        if(admin.validity===false){
            browserHistory.push('/');
        }else{
            axios({
                method: 'GET',
                url: 'http://localhost:8080/getAllTotalWorkedForCurrentMonth',
                headers:{'Authorization': admin.token}
            }).then(data=>{
                employeeData=data.data;
                dispatch(dashboard(employeeData))
            })
        }

    }
}


export function loadAllEmployee(authentication) {
    return dispatch=>{
        axios({
            method:'GET',
            url: 'http://localhost:8080/getAllEmployee',
            headers:{'Authorization': authentication.token}
        }).then(data=>{
            dispatch(displayEmployee(data.data))
        })
    }
}




export function deleteEmployeeAction(id,auth){
    return dispatch=>{
        axios({
            method: 'DELETE',
            url: 'http://localhost:8080/removeEmployee/'+id,
            headers:{'Authorization': auth.token}
        }).then(userdata=>{
            console.log(userdata.status)
            if(userdata.status!==200){
                alert("Remove failed");
            }else{
                dispatch(deleteAction(id));
            }

        })
    }
}





export function createUser(user,authentication) {
    return dispatch => {
        axios.post('http://localhost:8080/createEmployee', {
                "name": user.name,
                "username": user.username,
                "password": user.password,
                "role": user.role
            }, {
                headers: {'Authorization': authentication.token

                }}

        ).then(response => {

                axios.post('http://localhost:8080/getuserid',{
                    "username":user.username
                }, {
                    headers:{'Authorization': authentication.token}
                }).then(userid=>{

                    let userpush={
                        id:"",
                        username:""
                    }
                    userpush.id=userid.data.id;
                    userpush.username=user.username;
                    dispatch(successRegistered(userpush));
                })
            },function(error){
                alert("Failed to create User")
            }
        )
        dispatch(success(user));
    }
}

export function getUserHistory(userDetails,authentication){
    return dispatch => {
        axios({
            method: 'GET',
            url: 'http://localhost:8080/getHistory/'+userDetails.id+'/'+userDetails.fromDate+'/'+userDetails.tillDate,
            headers:{'Authorization': authentication.token}
        }).then(userdata=>{
            dispatch(history(userdata.data))
        })
    }
}

function history(userHistory) { return { type: "EMPLOYEE_HISTORY", userHistory } }

function dashboard(workedMonthly) { return { type: "EMPLOYEE_STATUS", workedMonthly } }

function displayEmployee(employees) { return { type: "EMPLOYEE_LIST", employees } }

function deleteAction(id) { return { type: "REMOVE_EMPLOYEE", id } }

function successRegistered(user) {
    return { type: "ADD_USER", user }
}
function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }