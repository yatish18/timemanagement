import React,{Component} from "react"
import { connect } from "react-redux"
import {loadDashboard,deleteTimeSlot} from "../../../action/user_actions";

import "../Dashboard.css"

class DisplayStatus extends Component {

    componentDidMount(){
        this.props.loadDashboard(this.props.authentication);
    }

    deleteTime(id,user){
        console.log("delete in application",id)
        console.log("this.props",this.props);
        this.props.deleteTimeSlot(id,user);
    }

    render() {
       /* let hours=this.props.user_dashboard.hours;
        let min=this.props.user_dashboard.min;*/

       let totalWorkedCurrentMonth=Object.assign({},this.props.user_dashboard.totalWorkedCurrentMonth);
       let totalWorkedTodayObject=Object.assign({},this.props.user_dashboard.totalWorkedTodayObject);
       let timeSheet=Object.assign([],this.props.user_dashboard.timeSheetForToday);
        return <div>
            <div className={"monthly"}>Current Month {totalWorkedCurrentMonth.hours} Hours, {totalWorkedCurrentMonth.minute} minutes </div>
             <div className={"today"}>Today you worked {totalWorkedTodayObject.hours} Hours,{totalWorkedTodayObject.minute} minutes</div>
            <div className="TimeSheet">
                <h2>Your Today's Timesheet</h2>
                <table className="table table-condensed">
                    <thead>
                    <tr>
                        <th>Start Time</th>
                        <th>End Time</th>
                    </tr>
                    </thead>
                    <tbody>
                        {timeSheet.map(data=>{
                            return <tr key={data.id}>
                                <td>{data.startTime}
                                </td>
                                <td>{data.endTime}
                                </td>
                                <div className={"btn btn-danger"} onClick={()=>this.deleteTime(data.id,this.props.authentication)}>Remove</div>
                            </tr>
                        })}

                    </tbody>
                </table>
            </div>

        </div>
    }
}

const mapStateToProps = ({authentication, user_dashboard}) => ({authentication, user_dashboard});

const mapDispatchToProps = {
    loadDashboard,
    deleteTimeSlot
};

export default connect(mapStateToProps, mapDispatchToProps)(DisplayStatus);