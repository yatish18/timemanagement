import {createStore,combineReducers,applyMiddleware} from 'redux';
import {authentication} from '../reducers/authentication.reducer'
import {registration} from "../reducers/registration.reducer";
import {employeeList} from "../reducers/admin_dashboard";
import {adminBoard} from "../reducers/admin_dashboard";
import {timeSheetHistory} from "../reducers/admin_dashboard";
import { browserHistory } from 'react-router';
import { syncHistoryWithStore} from 'react-router-redux';
import { routerReducer } from 'react-router-redux';
import {user_dashboard} from "../reducers/user_dashboard_display.reducer";
import thunk from 'redux-thunk';
import logger from 'redux-logger';


const store = createStore(combineReducers({
            authentication,
            user_dashboard,
            adminBoard,
            employeeList,
            timeSheetHistory
            ,routing: routerReducer
        }),
        {},
        applyMiddleware(thunk, logger)
    );

export default store;
export const history = syncHistoryWithStore(browserHistory, store);
