import React,{Component} from "react"
import { connect } from "react-redux"
import {loadAdminDashboard} from "../../action/admin_action";
import {adminBoard} from "../../reducers/admin_dashboard";

class DisplayEmployee  extends Component {

    componentDidMount(){
        this.props.loadAdminDashboard(this.props.authentication);
    }

    render() {
        let timeSheet=Object.assign([],this.props.adminBoard);
       return(<div>
           <div className={"workingHours"}>
               <table className="table table-condensed">
                   <thead>
                   <tr>
                       <th>USERNAME</th>
                       <th>HOURS</th>
                       <th>MINUTES</th>
                   </tr>
                   </thead>
                   <tbody>
                   {timeSheet.map(data=>{
                       return <tr key={data.id}>
                           <td>{data.username}
                           </td>
                           <td>{data.hours}
                           </td>
                           <td>{data.minute}
                           </td>
                       </tr>
                   })}

                   </tbody>
               </table>
           </div>

       </div>)

    }

}

const mapStateToProps = ({authentication, adminBoard}) => ({authentication, adminBoard});

const mapDispatchToProps = {
    loadAdminDashboard
};


export default connect(mapStateToProps, mapDispatchToProps)(DisplayEmployee)