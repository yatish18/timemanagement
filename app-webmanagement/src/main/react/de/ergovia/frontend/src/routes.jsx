import React from 'react'
import {Route} from 'react-router';
import Login from "./component/login/login";
import User_dashboard from "./component/user_dashboard/Dashboard"
import Admin_dashboard from "./component/admin_dashboard/Admin_dashboard"
import AdminHistory from "./component/admin_dashboard/AdminHistory"
import UserHistory from "./component/user_dashboard/container/UserHistory"
import RegisterPage from "./component/admin_dashboard/RegisterPage"
import UserManagement from "./component/admin_dashboard/UserManagement"

export default (
    <Route>
        <Route path="/" component= {Login}/>
        <Route path="/User_dashboard" component={User_dashboard}/>
        <Route path="/Admin_dashboard" component={Admin_dashboard}/>
        <Route path="/register" component={RegisterPage} />
        <Route path="/Admin_dashboard/History" component={AdminHistory}/>
        <Route path="/User_dashboard/History" component={UserHistory}/>
        <Route path="/Admin_dashboard/Users" component={UserManagement}/>
    </Route>
);
