import React,{Component} from "react";
import {deleteEmployeeAction} from "../../action/admin_action";
import {connect} from "react-redux";
import {loadAllEmployee} from "../../action/admin_action";

class RemoveUser extends Component{
    componentDidMount(){
        this.props.loadAllEmployee(this.props.authentication);
    }

    deleteEmployee(id,auth){
        this.props.deleteEmployeeAction(id,auth);
    }

    render(){
        const employees=this.props.employeeList;
        return <div>
            <div>
                <h2>Remove User</h2>
                <div className={"list"}>
                    {employees.map(data=>{
                        return <tr key={data.id}>
                            <td className={"listUsername"}>{data.username}
                            </td>
                            <div className={"btn btn-danger listRemove"} onClick={()=>this.deleteEmployee(data.id,this.props.authentication)} >Remove</div>
                        </tr>
                    })}
                </div>
            </div>
        </div>
    }
}

const mapStateToProps = ({authentication,employeeList}) => ({authentication,employeeList});

const mapDispatchToProps = {
    deleteEmployeeAction,
    loadAllEmployee
};
export default connect(mapStateToProps,mapDispatchToProps)(RemoveUser)