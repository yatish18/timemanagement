import React,{ Component } from "react"
import Link from "react-router/es/Link";
import { connect } from 'react-redux';
import {authentication} from "../../reducers/authentication.reducer";
import {createUser} from "../../action/admin_action";
import {registration} from "../../reducers/registration.reducer";

class RegisterPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {
                name: '',
                username: '',
                password: '',
                role:''
            },
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    }
    handleDateChange(date) {
        this.setState({
            user: {
                ...this.state.user,
                dob: date
            }
        });

    }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({ submitted: true });
        const { user } = this.state;
        const auth = this.props.authentication;
        const { dispatch } = this.props;
        if (user.name && user.username && user.password && user.role) {
            dispatch(createUser(user,auth));
        }
    }

    render() {
        /*const { registering  } = this.props.registration;*/
        const { user, submitted } = this.state;
        return (
            <div className="col-md-6 col-md-offset-3">
                <h2>Register User</h2>
                <form name="form" onSubmit={this.handleSubmit}>
                    <div className={'form-group' + (submitted && !user.name ? ' has-error' : '')}>
                        <label htmlFor="name"> Name</label>
                        <input type="text" className="form-control register" name="name" value={user.name} onChange={this.handleChange} />
                        {submitted && !user.name &&
                        <div className="help-block"> Name is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.username ? ' has-error' : '')}>
                        <label htmlFor="username">Username</label>
                        <input type="text" className="form-control register" name="username" value={user.username} onChange={this.handleChange} />
                        {submitted && !user.username &&
                        <div className="help-block">username is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.password ? ' has-error' : '')}>
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control register" name="password" value={user.password} onChange={this.handleChange} />
                        {submitted && !user.password &&
                        <div className="help-block">Password is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.password ? ' has-error' : '')}>
                        <label htmlFor="password">Role</label>
                        <select type="password" className="form-control register" name="role" value={user.role} onChange={this.handleChange}>
                        <option></option>
                        <option value={"ROLE_ADMIN"}>Admin</option>
                        <option value={"USER"}>User</option>
                        </select>
                        {submitted && !user.role &&
                        <div className="help-block">Role is required</div>
                        }
                    </div>
                    <div className="form-group">
                        <button className="btn btn-primary">Register</button>
                        <Link to="/Admin_dashboard" className="btn btn-link">Cancel</Link>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = ({authentication}) => ({authentication});

export default connect(mapStateToProps)(RegisterPage)