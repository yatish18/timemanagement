import {bake_cookie,read_cookie} from "sfcookies"

export function adminBoard(state = {}, action) {
    switch (action.type) {
        case "EMPLOYEE_STATUS":
            return Object.assign({},action.workedMonthly);
        default:
            return state;
    }
}

const refreshAfterDelete=(state,id)=>{
    console.log("refresh",state)
    state=state.filter(employee=> employee.id!==id)
    return state;
}
const refreshAfterAdding=(user)=>{
    let {id,username}= user;
    return {
        id,username
    }
}

export function employeeList(state=[],action) {
    let list= null;
    //state=read_cookie("EmployeeList");
    switch (action.type){
        case "EMPLOYEE_LIST":
            list=Object.assign([],action.employees);
            bake_cookie("EmployeeList",list);
            return list
        case "REMOVE_EMPLOYEE":
            list = refreshAfterDelete(state,action.id);
            bake_cookie("EmployeeList",list)
            return list;
        case "ADD_USER":
            list = [...state,refreshAfterAdding(action.user)]
            bake_cookie("EmployeeList",list)
            return list;
        default:
            return state;
    }

}

export function timeSheetHistory(state=[],action) {

    switch(action.type){
        case "EMPLOYEE_HISTORY":

            return action.userHistory;
        default:
            return state;
    }
}