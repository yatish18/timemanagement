import {userConstants} from "../constants/user.constants";
import {browserHistory} from 'react-router';
import axios from "axios/index";

let sendUser={
    id:"",
    username:"",
    token:"",
    role:"",
    validity:false
}
export function checkUserLogin(user) {
    return dispatch => {
        axios({
            method: 'GET',
            origin: "*",
            url: 'http://localhost:8080/login/'+user.username+'/'+user.password,
        }).then(userDetails => {
                    // login successful if there's object the response
                sendUser.token = userDetails.data.token[0];
                if (user.token !== " ") {
                    console.log(' user :', JSON.stringify(user));
                    // store user details in local storage
                    localStorage.setItem('user', JSON.stringify(user));
                    axios.post('http://localhost:8080/getuserid',{
                        "username":user.username
                    }, {
                        headers:{'Authorization': sendUser.token}
                    }).then(userid=>{
                        sendUser.id = userid.data.id;
                        sendUser.username = user.username;
                        sendUser.role = userid.data.role;
                        sendUser.validity = true;
                        dispatch(success(sendUser));

                        if(sendUser.role === "ROLE_ADMIN"){
                            browserHistory.push('/Admin_dashboard');
                        }else {
                            browserHistory.push('/User_dashboard');
                        }
                    })
                }
                },
                function(error){
                   dispatch(failure(error))
                }
            );
    };



    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}
export function logout(){
    return dispatch=>{
        dispatch(logoutUser());
        browserHistory.push('/');
    }
    function logoutUser() {  return { type: userConstants.LOGOUT } }
}

