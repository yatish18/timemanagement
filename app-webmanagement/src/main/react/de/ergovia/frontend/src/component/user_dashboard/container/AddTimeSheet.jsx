import React,{Component} from "react";
import {connect} from "react-redux";
import {addNewTimeSheet} from "../../../action/user_actions";

class AddTimeSheet extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: '',
            startTime: '',
            endTime:''
        };


    }
    submit() {
        addNewTimeSheet(this.state,this.props.authentication)
    }

    getDate(){
        var date=new Date()
        var yyyy=date.getFullYear();
        var dd=date.getDate();
        var mm=date.getMonth()+1;
        if(dd<10){
            dd='0'+dd;
        }
        if(mm<10){
            mm='0'+mm;
        }
        var formattedDate=yyyy+"-"+mm+"-"+dd;
        return formattedDate;
    }

    getFirstDayOfMonth(){
        var date=new Date();
        var y = date.getFullYear();
        var m = date.getMonth();
        var firstDay = new Date(y, m, 1);
        var yyyy=firstDay.getFullYear();
        var dd=firstDay.getDate();
        var mm=firstDay.getMonth()+1;
        if(dd<10){
            dd='0'+dd;
        }
        if(mm<10){
            mm='0'+mm;
        }
        var formattedDate=yyyy+"-"+mm+"-"+dd;
         return formattedDate;
    }

    reorderDate(date){
        date=new Date(date)
        var yyyy=date.getFullYear();
        var dd=date.getDate();
        var mm=date.getMonth()+1;
        if(dd<10){
            dd='0'+dd;
        }
        if(mm<10){
            mm='0'+mm;
        }
        var formattedDate=dd+"/"+mm+"/"+yyyy;
        return formattedDate;
    }
    render(){
        return(
        <div>
            <h2>Add new TimeSheet</h2>
            <input type="date" name="date"  min={this.getFirstDayOfMonth()} max={this.getDate()} onChange={event => this.setState({date: this.reorderDate(event.target.value)})} />
                <h3>Start Time</h3>
                <input type={"time"}   name={"startTime"} onChange={event => this.setState({startTime: event.target.value})} />
                <h3>End Time</h3>
                <input type={"time"}  name={"endTime"} onChange={event => this.setState({endTime: event.target.value})}/>
            <button className={"btn btn-success"} onClick={()=>this.submit()}>Submit</button>
        </div>

        )
    }
}

const mapStateToProps = ({authentication, user_dashboard}) => ({authentication, user_dashboard});

const mapDispatchToProps = {
    addNewTimeSheet
};

export default  connect(mapStateToProps, mapDispatchToProps)(AddTimeSheet)