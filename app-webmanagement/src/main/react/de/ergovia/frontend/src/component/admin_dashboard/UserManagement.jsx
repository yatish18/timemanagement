import React,{Component} from "react";
import RegisterPage from "./RegisterPage";
import RemoveUser from "./RemoveUser";
import {browserHistory} from "react-router";
class UserManagement extends Component{
    render(){
        return(<div>
            <div className={"usermanagement-title"}>
                <div>
                    <button className={"btn btn-primary userToDashBoard"}onClick={ ()=>browserHistory.push('/Admin_dashboard')}>DashBoard</button>
                </div>
               <h1>User Management</h1>
            </div>
            <div className={"container"}>
                <div className={"registerPage"}><RegisterPage/></div>
                <div className={"removeUser"} ><RemoveUser/></div>
            </div>

           </div>)
    }

}
export default UserManagement