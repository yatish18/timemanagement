import React,{Component} from "react";
import {browserHistory} from "react-router";
import {logout} from "../../../action/login_actions";
import {connect} from "react-redux";
import {loadAdminDashboard,getUserHistory} from "../../../action/admin_action";

class UserHistory extends Component{
    constructor(props){
        super(props)
        this.state={
            id:this.props.authentication.id,
            fromDate:"",
            tillDate:""
        }
    }

    getDate(){
        var date=new Date()
        var yyyy=date.getFullYear();
        var dd=date.getDate();
        var mm=date.getMonth()+1;
        if(dd<10){
            dd='0'+dd;
        }
        if(mm<10){
            mm='0'+mm;
        }
        var formattedDate=yyyy+"-"+mm+"-"+dd;
        return formattedDate;
    }

    reorderDate(date){
        date=new Date(date)
        var yyyy=date.getFullYear();
        var dd=date.getDate();
        var mm=date.getMonth()+1;
        if(dd<10){
            dd='0'+dd;
        }
        if(mm<10){
            mm='0'+mm;
        }
        var formattedDate=dd+"-"+mm+"-"+yyyy;
        return formattedDate;
    }

    submit(){
        console.log(this.state)
        this.props.getUserHistory(this.state,this.props.authentication)
    }
    render(){
        let employee=Object.assign([],this.props.adminBoard);
        let timeHistory= this.props.timeSheetHistory || [];
        return(
            <div>
                <div>
                    <button className={"btn btn-primary dashboard"}onClick={ ()=>browserHistory.push('/User_dashboard')}>DashBoard</button>
                </div>
            <div className="History">

                <div className={"History-Title"}>
                    TimeSheet Browser</div>
            <div className={"form-inline"}>
                    <div className={"date"}>
                        <label>From</label>
                        <input className={"form-control"}  type={"date"}  max={this.getDate()}  onChange={event => this.setState({fromDate: this.reorderDate(event.target.value)})}/>
                    </div>
                    <div className={"date1"}>
                        <label>Till</label>
                        <input className={"form-control"}  type={"date"}  max={this.getDate()}  onChange={event => this.setState({tillDate: this.reorderDate(event.target.value)})}/>
                </div>
                <div>
                    <button className={"btn btn-success submit"} onClick={()=>this.submit()}>Submit</button>
                </div>
            </div>
            </div>
                <div>
                    <table className="table table-condensed">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>TimeSlot</th>
                            <th>Hours</th>
                            <th>Minutes</th>
                        </tr>
                        </thead>
                        <tbody>
                        {timeHistory.map(data=>{
                            return <tr key={data.date}>
                                <td>{data.date}
                                </td>
                                <td>{data.timeSheet.map(time=>{
                                    return <tr><td>{time.startTime}-{time.endTime}</td></tr>
                                    }
                                )}
                                </td>
                                <td>{data.hours}
                                </td>
                                <td>{data.minutes}
                                </td>
                            </tr>
                        })}

                        </tbody>
                    </table>
                </div>
            </div>
        )
    }

}
const mapStateToProps = ({authentication, timeSheetHistory}) => ({authentication, timeSheetHistory});

const mapDispatchToProps = {
    getUserHistory
};

export default connect(mapStateToProps,mapDispatchToProps)(UserHistory)