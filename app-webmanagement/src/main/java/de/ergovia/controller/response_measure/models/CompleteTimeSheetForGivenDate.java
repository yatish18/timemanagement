package de.ergovia.controller.response_measure.models;


import java.util.List;

public class CompleteTimeSheetForGivenDate {
    private String date;
    private List<TimeSheetEntity> timeSheet;

    public CompleteTimeSheetForGivenDate(String date, List<TimeSheetEntity> timeSheet) {
        this.date = date;
        this.timeSheet = timeSheet;
    }

    public CompleteTimeSheetForGivenDate() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<TimeSheetEntity> getTimeSheet() {
        return timeSheet;
    }

    public void setTimeSheet(List<TimeSheetEntity> timeSheet) {
        this.timeSheet = timeSheet;
    }
}
