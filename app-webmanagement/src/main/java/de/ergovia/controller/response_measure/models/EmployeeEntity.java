package de.ergovia.controller.response_measure.models;


import java.util.Set;

public class EmployeeEntity {


    private String id;

    private String username;
    private Set<TimeSheetEntity> timeSheets;


    public EmployeeEntity(String id, String username) {
        this.id = id;
        this.username = username;
    }

    public EmployeeEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setName(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "EmployeeEntity{" +
                "id=" + id +
                ", name='" + username + '\'' +
                ", products=" + timeSheets +
                '}';
    }
}
