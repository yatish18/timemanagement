package de.ergovia.controller.response_measure;

import de.ergovia.controller.response_measure.models.ClubbedData;
import de.ergovia.controller.response_measure.models.CompleteTimeSheetForGivenDate;
import de.ergovia.controller.response_measure.models.TotalWorkedByEmployee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

/**
 * For measuring GraphQL Response Time.
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
@RestController
public class MeasurementController {

    private RestTemplate restTemplate;
    private String serverURI;

    @Autowired
    public MeasurementController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.serverURI = "http://localhost:8081";
    }

    public String getServerURI() {
        return serverURI;
    }

    @GetMapping("/api/runRestTest")
    public ResponseEntity restTest(){
        ClubbedData data= new ClubbedData(totalWorkedTillNowByAllEmployeeMethod(),totalWorkedStartOfCurrentMonthMethod(),totalWorkedWithDateRangeMethod(),timeSheetForGivenDateMethod());
        return new ResponseEntity(data,HttpStatus.OK);
    }

    List<TotalWorkedByEmployee> totalWorkedTillNowByAllEmployeeMethod(){

        String totalWorkedTillNowByAllEmployee = String.format("%s/api/admin/totalWorkedTillNowByAllEmployee", this.getServerURI());
        ResponseEntity<TotalWorkedByEmployee[]> data= restTemplate.getForEntity(totalWorkedTillNowByAllEmployee,TotalWorkedByEmployee[].class);
        List<TotalWorkedByEmployee> test=Arrays.asList(data.getBody());
        return test;
    }

    TotalWorkedByEmployee totalWorkedStartOfCurrentMonthMethod(){
        String id="3a7d567b-827a-4cf9-a2fe-375f8bd6b4d2";
        String totalWorkedStartOfCurrentMonth = String.format("%s/totalWorkForCurrentMonth/"+id, this.getServerURI());
        ResponseEntity<TotalWorkedByEmployee> data= restTemplate.getForEntity(totalWorkedStartOfCurrentMonth,TotalWorkedByEmployee.class);

        return data.getBody();
    }

    TotalWorkedByEmployee totalWorkedWithDateRangeMethod(){
        String id="424917e1-c624-4495-b2b9-f9ac7af4f69b";
        String fromWhen="12-04-2018";
        String tillWhen="12-04-2018";
        String totalWorkedWithDateRange = String.format("%s/totalWorkedWithDateRange/"+id+"/"+fromWhen+"/"+tillWhen, this.getServerURI());
        ResponseEntity<TotalWorkedByEmployee> data= restTemplate.getForEntity(totalWorkedWithDateRange,TotalWorkedByEmployee.class);
        return data.getBody();
    }

    CompleteTimeSheetForGivenDate[] timeSheetForGivenDateMethod(){
        String id="424917e1-c624-4495-b2b9-f9ac7af4f69b";
        String fromWhen="12-04-2018";
        String tillWhen="12-04-2018";
        String timeSheetForGivenDate = String.format("%s/api/timesheetWithRange/"+id+"/"+fromWhen+"/"+tillWhen, this.getServerURI());
        ResponseEntity<CompleteTimeSheetForGivenDate[]> data= restTemplate.exchange(timeSheetForGivenDate,HttpMethod.GET,null,CompleteTimeSheetForGivenDate[].class);
        return data.getBody();
    }
}
