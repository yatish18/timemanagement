package de.ergovia.controller.response_measure.models;

import java.util.List;

public class ClubbedData {
    private List<TotalWorkedByEmployee> totalWorkedTillNowByAllEmployeep;
    private TotalWorkedByEmployee totalWorkedStartOfCurrentMonth;
    private TotalWorkedByEmployee totalWorkedWithDateRange;
    private CompleteTimeSheetForGivenDate[] timeSheetForGivenDate;

    public ClubbedData(List<TotalWorkedByEmployee> totalWorkedTillNowByAllEmployeep, TotalWorkedByEmployee totalWorkedStartOfCurrentMonth, TotalWorkedByEmployee totalWorkedWithDateRange, CompleteTimeSheetForGivenDate[] timeSheetForGivenDate) {
        this.totalWorkedTillNowByAllEmployeep = totalWorkedTillNowByAllEmployeep;
        this.totalWorkedStartOfCurrentMonth = totalWorkedStartOfCurrentMonth;
        this.totalWorkedWithDateRange = totalWorkedWithDateRange;
        this.timeSheetForGivenDate = timeSheetForGivenDate;
    }

    public List<TotalWorkedByEmployee> getTotalWorkedTillNowByAllEmployeep() {
        return totalWorkedTillNowByAllEmployeep;
    }

    public void setTotalWorkedTillNowByAllEmployeep(List<TotalWorkedByEmployee> totalWorkedTillNowByAllEmployeep) {
        this.totalWorkedTillNowByAllEmployeep = totalWorkedTillNowByAllEmployeep;
    }

    public TotalWorkedByEmployee getTotalWorkedStartOfCurrentMonth() {
        return totalWorkedStartOfCurrentMonth;
    }

    public void setTotalWorkedStartOfCurrentMonth(TotalWorkedByEmployee totalWorkedStartOfCurrentMonth) {
        this.totalWorkedStartOfCurrentMonth = totalWorkedStartOfCurrentMonth;
    }

    public TotalWorkedByEmployee getTotalWorkedWithDateRange() {
        return totalWorkedWithDateRange;
    }

    public void setTotalWorkedWithDateRange(TotalWorkedByEmployee totalWorkedWithDateRange) {
        this.totalWorkedWithDateRange = totalWorkedWithDateRange;
    }

    public CompleteTimeSheetForGivenDate[] getTimeSheetForGivenDate() {
        return timeSheetForGivenDate;
    }

    public void setTimeSheetForGivenDate(CompleteTimeSheetForGivenDate[] timeSheetForGivenDate) {
        this.timeSheetForGivenDate = timeSheetForGivenDate;
    }
}