package de.ergovia.controller.response_measure.models;

public class TotalWorkedByEmployee {
    private String employeeId;
    private int hours;
    private int minute;

    public TotalWorkedByEmployee() {
    }

    public TotalWorkedByEmployee(String employeeId, int hours, int minute) {
        this.employeeId = employeeId;
        this.hours = hours;
        this.minute = minute;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }
}
