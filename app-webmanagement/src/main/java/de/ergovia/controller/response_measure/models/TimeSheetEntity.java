package de.ergovia.controller.response_measure.models;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class TimeSheetEntity {

    private int id;
    private EmployeeEntity employee;
    private String date;
    private String startTime;
    private String endTime;
    private SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
    private SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");


    public TimeSheetEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EmployeeEntity getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeEntity employee) {
        this.employee = employee;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) throws ParseException {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) throws ParseException {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) throws ParseException {
        this.endTime = endTime;
    }
}
