package de.ergovia.controller.RestAPI.model;

public class SendUserName {
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
