package de.ergovia.controller.RestAPI.utility;

import de.ergovia.controller.RestAPI.model.PasswordVerifyForAuth;

public class ModelConverter {
    public static PasswordVerifyForAuth convertUsernameAndPasswordToPasswordVerifyForAuth(String username,String password){
        return new PasswordVerifyForAuth(username,password);
    }
}
