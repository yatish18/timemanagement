package de.ergovia.controller.RestAPI.utility;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;

@ResponseStatus(HttpStatus.CONFLICT)
public class HttpClientErrorAllreadyExisits extends HttpClientErrorException {

    public HttpClientErrorAllreadyExisits(HttpStatus statusCode) {
        super(statusCode);
    }
}
