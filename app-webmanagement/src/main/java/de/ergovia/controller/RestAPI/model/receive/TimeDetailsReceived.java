package de.ergovia.controller.RestAPI.model.receive;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeDetailsReceived {
    private String startTime;
    private String endTime;


    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) throws ParseException {

        this.startTime =  startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) throws ParseException {

        this.endTime =  endTime;
    }
}
