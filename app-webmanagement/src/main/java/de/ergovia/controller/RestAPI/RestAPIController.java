package de.ergovia.controller.RestAPI;

import de.ergovia.controller.RestAPI.model.AuthorizationToken;
import de.ergovia.controller.RestAPI.model.SendUserName;
import de.ergovia.controller.RestAPI.model.receive.CreateNewEmployee;
import de.ergovia.controller.RestAPI.model.receive.EmployeeTimeSheetForm;
import de.ergovia.controller.RestAPI.utility.HttpClientErrorAllreadyExisits;
import de.ergovia.controller.RestAPI.utility.ModelConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import java.text.ParseException;
import java.util.List;

/**
 * Controller for Front-end  access
 *
 * @author  Yatish Kumar Ramakrishna
 * @version 1.0
 * @since   2018-05-11
 */
@RestController
public class RestAPIController {
    /**
     * RestTemplate for requesting backend.
     */
    private RestTemplate restTemplate;
    /**
     * Auth management URL.
     */
    private String serverURI;
    /**
     * Time Sheet URL.
     */
    private String timeSheetServer;

    @Autowired
    public RestAPIController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.serverURI = "http://localhost:8083";
        this.timeSheetServer ="http://localhost:8081";

    }

    public String getServerURI() {
        return serverURI;
    }

    public String getTimeSheetServer() {
        return timeSheetServer;
    }

    /**
     * Login Request for backend.
     * @param username Employee username.
     * @param password Employee password.
     * @return
     */
    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/login/{username}/{password}")
    public ResponseEntity adminLogin(@PathVariable String username, @PathVariable String password){
        String format = String.format("%s/login", this.getServerURI());
        try {
        return new ResponseEntity(new AuthorizationToken(restTemplate.postForEntity(format, new HttpEntity<>(ModelConverter.convertUsernameAndPasswordToPasswordVerifyForAuth(username,password)),HttpStatus.class).getHeaders().get("Authorization")),HttpStatus.OK);
        }catch (HttpClientErrorException err){
            throw new HttpClientErrorUnauthorized(err.getStatusCode());
        }
    }

    /**
     * Requesting userid from backend.
     * @param username employee username.
     * @param headers Token.
     * @return user id.
     */
    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/getuserid")
    public ResponseEntity getUserId(@RequestBody SendUserName username, @RequestHeader HttpHeaders headers){
        System.out.println(username.getUsername());
        String format = String.format("%s/api/user_id", this.getServerURI());
       try {
            return restTemplate.postForEntity(format, new HttpEntity<SendUserName>(username, headers), String.class);
        }catch (HttpClientErrorException err){
            throw new HttpClientErrorUnauthorized(err.getStatusCode());
        }
    }

    /**
     * Creating new employee.
     * @param employee employee details.
     * @param headers token.
     * @return Status.
     */
    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/createEmployee")
    public ResponseEntity createNewEmployee(@RequestBody CreateNewEmployee employee, @RequestHeader HttpHeaders headers){
        String format = String.format("%s/api/admin/createNewEmployee", this.getServerURI());
        try {
            return restTemplate.postForEntity(format, new HttpEntity<>(employee, headers), String.class);
        }catch (HttpClientErrorException err){
            if(err.getStatusCode().equals(HttpStatus.CONFLICT))
            {
                throw new HttpClientErrorAllreadyExisits(err.getStatusCode());
            }
                throw new HttpClientErrorUnauthorized(err.getStatusCode());
        }
    }

    /**
     * Request for Creating new time sheet
     * @param form timesheet form
     * @param headers token
     * @return status
     */
    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/submitNewSheet")
    public ResponseEntity createNewTimeSheet(@RequestBody EmployeeTimeSheetForm form, @RequestHeader HttpHeaders headers){
        String format = String.format("%s/newSheet", this.getTimeSheetServer());
        try {
            return restTemplate.postForEntity(format, new HttpEntity<>(form, headers), String.class);
        }catch (HttpClientErrorException err){
            throw new HttpClientErrorUnauthorized(err.getStatusCode());
        }
    }

    /**
     * Request for getting total worked time for current month.
     * @param id Employee unique id
     * @param headers token
     * @return total time.
     */
    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/getTotalWorked/{id}")
    public ResponseEntity getUserWorkedForCurrentMonth(@PathVariable String id, @RequestHeader HttpHeaders headers){
        List<String> token=headers.get("Authorization");
        String format = String.format("%s/totalWorkForCurrentMonth/"+id, this.getTimeSheetServer());
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        return restTemplate.exchange(format, HttpMethod.GET, entity,String.class);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/timesheetForOneDay/{id}/{date}")
    public ResponseEntity getUserTimeSheetForToday(@PathVariable String id,@PathVariable String date, @RequestHeader HttpHeaders headers) throws ParseException {
        String format = String.format("%s/api/timesheetForOneDay/"+id+"/"+date, this.getTimeSheetServer());
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        return restTemplate.exchange(format, HttpMethod.GET, entity,String.class);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @DeleteMapping("/api/deleteTimesheet/{id}")
    public ResponseEntity deleteTimeSheetFor(@PathVariable String id, @RequestHeader HttpHeaders headers) throws ParseException {
        String format = String.format("%s/api/deleteTimesheet/"+id, this.getTimeSheetServer());
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        return restTemplate.exchange(format, HttpMethod.DELETE, entity,HttpStatus.class);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/getTotalWorkedForToday/{id}/{date}")
    public ResponseEntity getUserWorkedToday(@PathVariable String id,@PathVariable String date, @RequestHeader HttpHeaders headers) throws ParseException {
        String format = String.format("%s/totalHoursPerDay/"+id+"/"+date, this.getTimeSheetServer());
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        return restTemplate.exchange(format, HttpMethod.GET, entity,String.class);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/getAllTotalWorkedForCurrentMonth")
    public ResponseEntity getAllUserWorkedCurrentMonth(@RequestHeader HttpHeaders headers) throws ParseException {
        String format = String.format("%s/api/admin/totalWorkedTillNowByAllEmployee", this.getTimeSheetServer());
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        return restTemplate.exchange(format, HttpMethod.GET, entity,String.class);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/getAllEmployee")
    public ResponseEntity getAllEmployee(@RequestHeader HttpHeaders headers) throws ParseException {
        String format = String.format("%s/api/admin/employeeList", this.getServerURI());
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        return restTemplate.exchange(format, HttpMethod.GET, entity,String.class);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @DeleteMapping("/removeEmployee/{username}")
    public ResponseEntity removeEmployee(@PathVariable String username,@RequestHeader HttpHeaders headers) throws ParseException {
        System.out.println("here");
        String format = String.format("%s/api/admin/removeEmployee/"+username, this.getServerURI());
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        return restTemplate.exchange(format, HttpMethod.DELETE, entity,String.class);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/getHistory/{id}/{fromWhen}/{tillWhen}")
    public ResponseEntity getTimeSheetByBate(@PathVariable String id, @PathVariable String fromWhen,@PathVariable String tillWhen,@RequestHeader HttpHeaders headers){
        List<String> token=headers.get("Authorization");
        String format = String.format("%s/api/timesheetWithRange/"+id+"/"+fromWhen+"/"+tillWhen, this.getTimeSheetServer());
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        return restTemplate.exchange(format, HttpMethod.GET, entity,String.class);
    }

}
