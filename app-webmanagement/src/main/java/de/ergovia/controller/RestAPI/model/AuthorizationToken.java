package de.ergovia.controller.RestAPI.model;

import java.util.ArrayList;
import java.util.List;

public class AuthorizationToken {
    private List<String> token;

    public AuthorizationToken(List<String> token) {
        this.token = token;
    }

    public List<String> getToken() {
        return token;
    }

    public void setToken(List<String> token) {
        this.token = token;
    }
}
