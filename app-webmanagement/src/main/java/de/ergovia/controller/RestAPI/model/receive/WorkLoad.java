package de.ergovia.controller.RestAPI.model.receive;


import java.text.ParseException;

import java.util.List;

public class WorkLoad {
    private String date;
    private TimeDetailsReceived timeDetails;

    public WorkLoad() {
    }

    public WorkLoad(String date, TimeDetailsReceived timeDetails) {
        this.date = date;
        this.timeDetails = timeDetails;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) throws ParseException {

        this.date = date;
    }

    public TimeDetailsReceived getTimeDetails() {
        return timeDetails;
    }

    public void setTimeDetails(TimeDetailsReceived timeDetails) {
        this.timeDetails = timeDetails;
    }
}
