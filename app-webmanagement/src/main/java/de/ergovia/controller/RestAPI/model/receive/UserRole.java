package de.ergovia.controller.RestAPI.model.receive;

public enum UserRole {
    ROLE_ADMIN,
    USER
}
